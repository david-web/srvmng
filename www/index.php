<?php

//phpinfo();//exit;

if( !ini_get('date.timezone') )
    date_default_timezone_set('Europe/London');


if(isset($_SERVER['env_type']) && $_SERVER['env_type'] == 'dev'){
	define('MYSQL_HOST', 'localhost');
	define('MYSQL_USER', 'root');
	define('MYSQL_PASSWORD', '');
	define('MYSQL_DB', 'srvmng');
	define('IS_DEV', '1');
	define('MAIN_URL', 'http://test2.ru');
	define('MAIN_DOMAIN', 'test2.ru');
        define('TOR_PROXY', '127.0.0.1:9150');


$random_ip=array(
'88.181.16.26', '46.16.33.70', '86.129.152.246', '185.19.21.160'
#'31.3.243.219', '213.138.115.18', '190.6.83.163', '185.19.21.160'
                );
srand((double) time()/(60*60));
$_SERVER['REMOTE_IP']=$_SERVER['HTTP_X_REAL_IP']=$random_ip[Rand(0,count($random_ip)-1)];

	// remove the following lines when in production mode
	defined('YII_DEBUG') or define('YII_DEBUG',true);
	// specify how many levels of call stack should be shown in each log message
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
}
else if(isset($_SERVER['env_type']) && $_SERVER['env_type'] == 'prod'){
	define('MYSQL_HOST', '127.0.0.1');
	define('MYSQL_USER', 'srvmng');
	define('MYSQL_PASSWORD', '');
	define('MYSQL_DB', 'srvmng');
	define('PRODUCTION', '1');
	define('MAIN_URL', 'http://r7.onion');
	define('MAIN_DOMAIN', 'r7.onion');
        define('TOR_PROXY', '127.0.0.1:9050');
}
else{
        define('MYSQL_HOST', '127.0.0.1');
	define('MYSQL_USER', 'srvmng');
	define('MYSQL_PASSWORD', '');
	define('MYSQL_DB', 'srvmng');
	define('PRODUCTION', '1');
	define('MAIN_URL', 'http://r7.onion');
	define('MAIN_DOMAIN', 'r7.onion');
        define('TOR_PROXY', '127.0.0.1:9050');
}

// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';


require_once($yii);
Yii::createWebApplication($config)->run();
