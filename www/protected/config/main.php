<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Device management framework',

	// preloading 'log' component
	'preload'=>array('log'),
    
        // path aliases
        'aliases' => array(
            'bootstrap' => realpath(__DIR__ . '/../extensions/bootstrap'), // change this if necessary
            //'vendor.twbs.bootstrap.dist' => realpath(__DIR__.'/../vendor/twbs/bootstrap/dist'), // bootstrap 2.3.2
        ),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
                //-- bootstrap
                'bootstrap.behaviors.*',
                'bootstrap.components.*',
                'bootstrap.form.*',
                'bootstrap.gii.bootstrap.*',
                'bootstrap.helpers.*',
                'bootstrap.widgets.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		/*
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'gwagwa',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		*/
                'gii' => array(
                        'class'=>'system.gii.GiiModule',
                        'generatorPaths' => array('bootstrap.gii'),
                        'password'=>'gwagwa',
                        // If removed, Gii defaults to localhost only. Edit carefully to taste.
                        'ipFilters'=>array('127.0.0.1','::1'),
                ),
	),

	// application components
	'components'=>array(

                'bootstrap' => array(
                    'class' => 'bootstrap.components.TbApi',
                    'cdnUrl'=>"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/", // bootstrap 3.5.3
                ),
            
                'phpseclib' => array(
                    'class' => 'ext.phpseclib.PhpSecLib'
                ),
            
		'user'=>array(
                        'class' => 'WebUser',
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
                        'identityCookie' => array('domain' => '.'.MAIN_DOMAIN),
		),
            
                'session'=>array(
			'class'=>'system.web.CDbHttpSession',
			'connectionID'=>'db',
#			'sessionTableName' => 'yiisession',
			'cookieParams' => ['domain' => '.'.MAIN_DOMAIN],
			'timeout' => (3600*24*30),
			'autoCreateSessionTable' => true,
		),
            
                'authManager' => array(
                    // Будем использовать свой менеджер авторизации
                    'class' => 'PhpAuthManager',
                    // Роль по умолчанию. Все, кто не админы, модераторы и юзеры — гости.
                    'defaultRoles' => array('guest'),
                ),

		// uncomment the following to enable URLs in path-format
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		*/

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>YII_DEBUG ? null : 'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);
