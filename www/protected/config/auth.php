<?php
return array(
    'guest' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Guest',
        'bizRule' => null,
        'data' => null
    ),
    // auth user
    'operator' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Operator',
        'children' => array(
            'guest', // + guest
        ),
        'bizRule' => null,
        'data' => null
    ),
    'admin' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Administrator',
        'children' => array(
            'operator', // + guest
        ),
        'bizRule' => null,
        'data' => null
    ),
);
