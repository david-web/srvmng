<?php

// This is the database connection configuration.
return array(
	//'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
	// uncomment the following lines to use a MySQL database
	
        'connectionString' => 'mysql:host='.MYSQL_HOST.';dbname='.MYSQL_DB.'',
        'emulatePrepare' => true,
        'username' => MYSQL_USER,
        'password' => MYSQL_PASSWORD,
        'charset' => 'utf8',
        'schemaCacheID'=>'cache',
        'schemaCachingDuration'=>3600,
        'enableProfiling'=>true,
	//'enableParamLogging' => true,
        'class' => 'CDbConnection',
    
);