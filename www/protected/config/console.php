<?php


if((isset($_SERVER['HOMEDRIVE']) && $_SERVER['HOMEDRIVE'] == 'C:')
 || php_uname("n") == "L"){
	define('MYSQL_HOST', 'localhost');
	define('MYSQL_USER', 'root');
	define('MYSQL_PASSWORD', '');
	define('MYSQL_DB', 'srvmng');
	define('IS_DEV', '1');
	define('MAIN_URL', 'http://test2.ru');
	define('MAIN_DOMAIN', 'test2.ru');
        define('TOR_PROXY', '127.0.0.1:9150');
}
elseif(php_uname("n") == "srvmng"){
	define('MYSQL_HOST', '127.0.0.1');
	define('MYSQL_USER', 'srvmng');
	define('MYSQL_PASSWORD', 'srvmng$h8');
	define('MYSQL_DB', 'srvmng');
	define('PRODUCTION', '1');
	define('MAIN_URL', 'https://www.srvmng.com');
	define('MAIN_DOMAIN', 'srvmng.com');
        define('TOR_PROXY', '127.0.0.1:9050');
}
else{
        define('MYSQL_HOST', '127.0.0.1');
	define('MYSQL_USER', 'srvmng');
	define('MYSQL_PASSWORD', 'srvmng$h8');
	define('MYSQL_DB', 'srvmng');
	define('PRODUCTION', '1');
	define('MAIN_URL', 'https://www.srvmng.com');
	define('MAIN_DOMAIN', 'srvmng.com');
        define('TOR_PROXY', '127.0.0.1:9050');
}


// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',

	// preloading 'log' component
	'preload'=>array('log'),
    
        // autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	// application components
	'components'=>array(

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),
            
                'phpseclib' => array(
                    'class' => 'ext.phpseclib.PhpSecLib'
                ),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),

	),
);
