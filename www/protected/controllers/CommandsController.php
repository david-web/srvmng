<?php

//chdir(dirname(__DIR__));
require_once(dirname(__DIR__).'/vendor/autoload.php');

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

class CommandsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create'),
				'roles'=>array('operator'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','update','launch','launchn1'),
                                'roles'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
        
        public function actionTestRpc()
	{
	
                $connection = new AMQPConnection('localhost', 5672, 'guest', 'guest1789');
                $channel = $connection->channel();

                $channel->queue_declare('email_queue', false, false, false, false);

                $_POST = array('param1'=>'val11');
                
                $data = json_encode($_POST);

                $msg = new AMQPMessage($data, array('delivery_mode' => 2));
                $channel->basic_publish($msg, '', 'email_queue');
                
                echo "10 <br/>";
                
                /*
                $this->render('view',array(
			'model'=>$this->loadModel($id),
		));
                */
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Commands;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
		if(isset($_POST['Commands']))
		{
                        $model->attributes=$_POST['Commands'];
			if($model->save()){
                            if(isset($_POST['select_conn'])){
                                $model->refresh();
                            
                                foreach($_POST['select_conn'] as $connector){
                                    $com_conn = new CommandConnectors();
                                    $com_conn->id_command = $model->id;
                                    $com_conn->id_connector = $connector;
                                    $com_conn->save();
                                }
                            
                            }
                        }
                        $this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                $com_conns = $model->commandConnectors;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Commands']))
		{
			$model->attributes=$_POST['Commands'];
                        if(isset($_POST['run_as_adm'])){
                            $model->run_as_adm = 'Y';
                        }
                        else{
                            $model->run_as_adm = 'N';
                        }
                        
			if($model->save()){
                            if(isset($_POST['select_conn'])){
                                $model->refresh();
                            
                                foreach($_POST['select_conn'] as $connector){
                                    //--- add new
                                    $criteria = new CDbCriteria();
                                    $criteria->addCondition('id_command = :id_command AND id_connector = :id_connector');
                                    $criteria->params = Array(':id_command' => $model->id, ':id_connector' => $connector);

                                    $com_con_model = CommandConnectors::model()->find($criteria);
                                    if(isset($com_con_model->id) && $com_con_model->id > 0){
                                        continue;
                                    }
                                    
                                    $com_conn = new CommandConnectors();
                                    $com_conn->id_command = $model->id;
                                    $com_conn->id_connector = $connector;
                                    $com_conn->save();
                                    
                                }
                                
                                //--- remove unselected
                                $criteria = new CDbCriteria();
                                $criteria->addCondition('id_command = :id_command');
                                $criteria->params = Array(':id_command' => $model->id,);

                                $com_con_models = CommandConnectors::model()->findAll($criteria);
                                foreach($com_con_models as $com_con_model){
                                    if(isset($com_con_model->id) && 
                                            $com_con_model->id > 0 &&
                                            !in_array($com_con_model->id, $_POST['select_conn']))
                                    {
                                        $com_con_model->delete();
                                    }
                                }
                            
                            }
                        }
                        $this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
                        'com_conns'=>$com_conns,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Commands');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Commands('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Commands']))
			$model->attributes=$_GET['Commands'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Commands the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Commands::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Commands $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='commands-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        
        public function actionLaunch($id)
	{
		//var_dump($id);die();
                
                //$command_model=$this->loadModel($id);
                
                $console=new TConsoleRunner('console.php');
                //$console->run('AccessLog Test --id_command=1');
                $console->run('AccessLog Test --id_command='.intval($id));
                
                $this->redirect( Yii::app()->createUrl('commands/admin') );
                
                /*
		$model=new Commands('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Commands']))
			$model->attributes=$_GET['Commands'];
                
                
		$this->render('admin',array(
			'model'=>$model,
		));
                */
                
	}

        
        
}
