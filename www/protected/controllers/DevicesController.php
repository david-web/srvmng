<?php

class DevicesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','import','GetConnectorDetails'),
				'roles'=>array('operator'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','update','DeleteConnector'),
                                'roles'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //print_r($_REQUEST);die();
                $POST = self::strip_tags_array($_POST);
            
                $model=new Devices;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Devices']))
		{
                    $model->attributes=$_POST['Devices'];
                    if($model->save()){

                        if(isset($POST['connector'])){
                            $model->refresh();
                            foreach($POST['connector'] as $connector){
                                
                                $connector_model = Connectors::model()->findByPk($connector['id']);
                                if(!isset($connector_model->id)){
                                    continue;
                                }
                                
                                $connector_obj = DevicesConnectors::model()->findByAttributes(
                                        Array(
                                            'id_device'=>$model->id,
                                            'id_connector'=>$connector['id'],
                                            'port'=>$connector['port'],
                                            )
                                );
                                if(isset($connector_obj->id) && $connector_obj->id >0){
                                    switch ($connector_model->type) {
                                        case 'telnet':
                                            $con_telnet_param_obj = ConnectorTelnetParams::model()->findByAttributes(
                                                    Array(
                                                        'id_device'=>$model->id,
                                                        'login'=>$connector['login'],
                                                        )
                                            );
                                            
                                            if(!isset($con_telnet_param_obj->id) || $con_telnet_param_obj->id < 1){
                                                $con_telnet_param_obj = new ConnectorTelnetParams();
                                                $con_telnet_param_obj->id_device = $model->id;
                                                $con_telnet_param_obj->login = $connector['login'];
                                                $con_telnet_param_obj->pass = $connector['pass'];
                                                $con_telnet_param_obj->save();
                                            }
                                            elseif($con_telnet_param_obj->pass != $connector['pass']){
                                                $con_telnet_param_obj->pass = $connector['pass'];
                                                $con_telnet_param_obj->save();
                                            }

                                            break;
                                        case 'ssh':
                                            $con_ssh_param_obj = ConnectorSshParams::model()->findByAttributes(
                                                    Array(
                                                        'id_device'=>$model->id,
                                                        'login'=>$connector['login'],
                                                        )
                                            );
                                            
                                            if(!isset($con_ssh_param_obj->id) || $con_ssh_param_obj->id < 1){
                                                $con_ssh_param_obj = new ConnectorSshParams();
                                                $con_ssh_param_obj->id_device = $model->id;
                                                $con_ssh_param_obj->login = $connector['login'];
                                                $con_ssh_param_obj->pass = $connector['pass'];
                                                $con_ssh_param_obj->save();
                                            }
                                            elseif($con_ssh_param_obj->pass != $connector['pass']){
                                                $con_ssh_param_obj->pass = $connector['pass'];
                                                $con_ssh_param_obj->save();
                                            }
                                            
                                            break;
                                    }
                                
                                    continue;
                                }
                                
                                $connector_obj = new DevicesConnectors();
                                $connector_obj->id_device = $model->id; 
                                $connector_obj->id_connector = $connector['id'];
                                $connector_obj->port = $connector['port'];
                                $connector_obj->save();
                                $connector_obj->refresh();
                                
                                switch ($connector_model->type) {
                                    case 'telnet':
                                        $con_telnet_param_obj = new ConnectorTelnetParams();
                                        $con_telnet_param_obj->id_device = $model->id;
                                        $con_telnet_param_obj->login = $connector['login'];
                                        $con_telnet_param_obj->pass = $connector['pass'];
                                        $con_telnet_param_obj->save();
                                        break;
                                    case 'ssh':
                                        $con_ssh_param_obj = new ConnectorSshParams();
                                        $con_ssh_param_obj->id_device = $model->id;
                                        $con_ssh_param_obj->login = $connector['login'];
                                        $con_ssh_param_obj->pass = $connector['pass'];
                                        $con_ssh_param_obj->save();
                                        break;
                                }
                                
                            }
                        }

                        $this->redirect(array('view','id'=>$model->id));

                    }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		//print_r($_REQUEST);die();
                
                $POST = self::strip_tags_array($_POST);
                $model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Devices']))
		{
                    $model->attributes=$_POST['Devices'];
                    if($model->save()){
                        if(isset($POST['connector'])){
                            $model->refresh();
                            foreach($POST['connector'] as $connector){

                                $connector_model = Connectors::model()->findByPk($connector['id']);
                                if(!isset($connector_model->id)){
                                    continue;
                                }

                                if(isset($connector['con_obj']) && $connector['con_obj'] >0){
                                    $connector_obj = DevicesConnectors::model()->findByPk($connector['con_obj']);
                                    if(!isset($connector_obj->id)){
                                        //-- can not find obj for given id. skip
                                        continue;
                                    }
                                    
                                    /*
                                    echo "current type=".$connector_obj->idConnector->type."</br>";
                                    echo "new type=".$connector_model->type."</br>";
                                    die();
                                    */
                                    
                                    //--- update existing connector
                                    if($connector_obj->idConnector->type != $connector_model->type){
                                        //-- type has changed, remove old connector data
                                        switch ($connector_obj->idConnector->type) {
                                            case 'telnet':
                                                $con_telnet_param_obj = ConnectorTelnetParams::model()->findByAttributes(
                                                        Array(
                                                            'id_device'=>$model->id,
                                                            )
                                                );
                                                $con_telnet_param_obj->delete();

                                                break;
                                            case 'ssh':
                                                $con_ssh_param_obj = ConnectorSshParams::model()->findByAttributes(
                                                        Array(
                                                            'id_device'=>$model->id,
                                                            )
                                                );
                                                $con_ssh_param_obj->delete();

                                                break;
                                        }
                                        
                                    }
                                    //--- update connector data
                                    switch ($connector_model->type) {
                                        case 'telnet':
                                            
                                            /*
                                            echo "current type=".$connector_obj->idConnector->type."</br>";
                                            echo "new type=".$connector_model->type."</br>";
                                            die();
                                            */
                                            
                                            $con_telnet_param_obj = ConnectorTelnetParams::model()->findByAttributes(
                                                    Array(
                                                        'id_device'=>$model->id,
                                                        )
                                            );
                                            
                                            if(!isset($con_telnet_param_obj->id) || $con_telnet_param_obj->id < 1){
                                                $con_telnet_param_obj = new ConnectorTelnetParams();
                                                $con_telnet_param_obj->id_device = $model->id;
                                            }
                                            
                                            $con_telnet_param_obj->login = $connector['login'];
                                            $con_telnet_param_obj->pass = $connector['pass'];
                                            $con_telnet_param_obj->save();

                                            break;
                                        case 'ssh':
                                            $con_ssh_param_obj = ConnectorSshParams::model()->findByAttributes(
                                                    Array(
                                                        'id_device'=>$model->id,
                                                        )
                                            );
                                            
                                            if(!isset($con_ssh_param_obj->id) || $con_ssh_param_obj->id < 1){
                                                $con_ssh_param_obj = new ConnectorSshParams();
                                                $con_ssh_param_obj->id_device = $model->id;
                                            }

                                            $con_ssh_param_obj->login = $connector['login'];
                                            $con_ssh_param_obj->pass = $connector['pass'];
                                            $con_ssh_param_obj->save();

                                            break;
                                    }
                                    
                                    $connector_obj->id_connector = $connector_model->id;
                                    $connector_obj->port = $connector['port'];
                                    $connector_obj->save();
                                }
                                else{
                                    //--- create new connectors
                                    $connector_obj = new DevicesConnectors();
                                    $connector_obj->id_device = $model->id; 
                                    $connector_obj->id_connector = $connector['id'];
                                    $connector_obj->port = $connector['port'];
                                    $connector_obj->save();
                                    $connector_obj->refresh();

                                    switch ($connector_model->type) {
                                        case 'telnet':
                                            $con_telnet_param_obj = new ConnectorTelnetParams();
                                            $con_telnet_param_obj->id_device = $model->id;
                                            $con_telnet_param_obj->login = $connector['login'];
                                            $con_telnet_param_obj->pass = $connector['pass'];
                                            $con_telnet_param_obj->save();
                                            break;
                                        case 'ssh':
                                            $con_ssh_param_obj = new ConnectorSshParams();
                                            $con_ssh_param_obj->id_device = $model->id;
                                            $con_ssh_param_obj->login = $connector['login'];
                                            $con_ssh_param_obj->pass = $connector['pass'];
                                            $con_ssh_param_obj->save();
                                            break;
                                    }
                                }

                            }
                        }
                        $this->redirect(array('view','id'=>$model->id));
                    }
                        
		}

		$this->render('update',array(
			'model'=>$model,
                        'dev_connectors' => $model->devicesConnectors,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionDeleteConnector()
	{
            $POST = self::strip_tags_array($_POST);
            $result = Array();
            
            if(!isset($POST['con_obj']) || $POST['con_obj'] < 1){
                $result['error'] = 'error: no con_obj_id';
                echo json_encode($result);
                return;
            }
            
            $dev_con_obj = DevicesConnectors::model()->findByPk($POST['con_obj']);
            if(!isset($dev_con_obj->id) || $dev_con_obj->id < 1){
                $result['error'] = 'error: no dev_con_obj found in db for id='.$POST['con_obj'];
                echo json_encode($result);
                return;
            }
            
            switch ($dev_con_obj->idConnector->type) {
                case 'telnet':
                    $con_telnet_param_obj = ConnectorTelnetParams::model()->findByAttributes(
                        Array(
                            'id_device'=>$dev_con_obj->id_device,
                            )
                    );
                    $con_telnet_param_obj->delete();

                    break;
                case 'ssh':
                    $con_ssh_param_obj = ConnectorSshParams::model()->findByAttributes(
                        Array(
                            'id_device'=>$dev_con_obj->id_device,
                            )
                    );
                    $con_ssh_param_obj->delete();

                    break;
            }
            $dev_con_obj->delete();
            $result['ok'] = 1;
            $result['html'] = TbHtml::alert(TbHtml::ALERT_COLOR_SUCCESS, 'Connector was successfully deleted.');
            
            echo json_encode($result);
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Devices');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Devices('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Devices']))
			$model->attributes=$_GET['Devices'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
        public function actionImport() {
            
            $model = new DevicesListFile;
            //$file = CUploadedFile::getInstance($model,'csv_file');
            if(isset($_POST['DevicesListFile']))
            {
                $model->attributes=$_POST['DevicesListFile'];
                if(!empty($_FILES['DevicesListFile']['tmp_name']['csv_file']))
                {
                    $file = CUploadedFile::getInstance($model,'csv_file');


                    $fp = fopen($file->tempName, 'r');
                    if($fp)
                    {
                        //  $line = fgetcsv($fp, 1000, ",");
                        //  print_r($line); exit;
                        $first_time = true;
                        
                     do {
                            if ($first_time == true) {
                                $first_time = false;
                                continue;
                            }
                            
                            //--- parse line
                            $url = trim($line[0]);
                            $connector_template = trim($line[1]);
                            
                            if(empty($url)){
                                $this->render('import',array(
                                        'model'=>$model,
                                        'host_error'=>'Empty url',
                                ));

                                Yii::app()->end();
                            }
                            
                            if(empty($connector_template)){
                                $this->render('import',array(
                                        'model'=>$model,
                                        'host_error'=>'Empty connector',
                                ));

                                Yii::app()->end();
                            }
                            
                            $criteria = new CDbCriteria();
                            $criteria->addCondition('name = :name');
                            $criteria->params = Array(':name' => $connector_template,);
                            $criteria->select = array('id');
                            
                            $connector_model = Connectors::model()->find($criteria);
                            
                            if(!isset($connector_model->id) || $connector_model->id < 1){
                                //--- can not resolve hostname to IP
                                $this->render('import',array(
                                        'model'=>$model,
                                        'host_error'=>'Wrong connector type',
                                ));

                                Yii::app()->end();
                            }
                            
                            //var_dump(parse_url($url));
                            //echo "</br>";
                            
                            $url_parts = parse_url($url);
                            
                            $protocol = $url_parts['scheme'];
                            $ip = $url_parts['host'];
                            
                            //var_dump($hostname);die();
                            
                            $port = 80;
                            if(isset($url_parts['port'])){
                                $port = $url_parts['port'];
                            }
                            
                            if(isset($url_parts['user'])){
                                $user = $url_parts['user'];
                            }
                            
                            if(isset($url_parts['pass'])){
                                $pass = $url_parts['pass'];
                            }
                            
                            if(isset($url_parts['path'])){
                                $path = $url_parts['path'];
                            }
                            
                            if(isset($url_parts['query'])){
                                $query = $url_parts['query'];
                            }
                            
                            if(isset($url_parts['fragment'])){
                                $fragment = $url_parts['fragment'];
                            }
                            
                            
                            if(!filter_var($ip, FILTER_VALIDATE_IP))
                            {
                                //--- can not resolve hostname to IP
                                $this->render('import',array(
                                        'model'=>$model,
                                        'host_error'=>'Not valid ip IP',
                                ));

                                Yii::app()->end();
                            }
                            
                            //---------- check if host with connector exists -------------------//
                            $criteria = new CDbCriteria();
                            $criteria->with = Array('devicesConnectors', 'devicesConnectors.idConnector');
                            $criteria->addCondition('ip = :ip AND devicesConnectors.port = :port AND idConnector.name = :connector');
                            $criteria->params = Array(':ip' => $ip, ':port' => $port, ':connector' => $connector_template);
                            $criteria->select = array('id');

                            $device = Devices::model()->find($criteria);
                            
                            //var_dump($host);die();
                            
                            //---------- check if device and connector exists -------------------//
                            if(isset($device->id) && $device->id > 0){
                                //-- device and connector already exists
                                continue;
                            }
                            
                            //---------- check if device exists -------------------//
                            $criteria = new CDbCriteria();
                            $criteria->addCondition('ip = :ip AND port = :port');
                            $criteria->params = Array(':ip' => $ip, ':port' => $port);
                            $criteria->select = array('id');

                            $device = Devices::model()->find($criteria);
                            if(!isset($device->id) || $device->id < 1){
                                $device = new Devices();
                                $device->ip = $ip;
                                $device->type = 'server';
                                $device->save();
                                $device->refresh();
                            }
                            
                            //--- add connector
                            $connector = new DevicesConnectors();
                            $connector->id_device = $device->id;
                            $connector->id_connector = $connector_model->id;
                            $connector->port = $port;
                            $connector->save();


                        }while( ($line = fgetcsv($fp, 1000, ";")) != FALSE);
                        
                        //$this->redirect('././index');

                    }
                    //    echo   $content = fread($fp, filesize($file->tempName));
                    //unlink($file->tempName);

                }

                $this->render('import',array(
                        'model'=>$model,
                        'import_log'=>'Import compleat',
                ));

                Yii::app()->end();
                
            }

            $this->render('import', array('model' => $model) );

        }

        public function strip_tags_array($arr = Array()){

		$result = '';

		foreach($arr AS $key => $elem){
			if(is_array($elem)){
				$result[$key] = self::strip_tags_array($elem);
				continue;
			}

			$result[$key] = (!empty($elem) ? strip_tags($elem) : '');
		}

		return $result;
	}
        
        public function actionGetConnectorDetails(){
            
            $POST = self::strip_tags_array($_POST);
            $result = Array();
            $data = Array();
            
            if(isset($POST['last_con_index'])){
                $POST['con_index'] = $POST['last_con_index'] + 1;
            }
            
            if(isset($POST['id_connector']) && $POST['id_connector'] > 0){
                $connector_obj = Connectors::model()->findByPk($POST['id_connector']);
                if(!isset($connector_obj->id) || $connector_obj->id < 1){
                    $result['error'] = 'error: can not find connector_obj for id='.$POST['id_connector'];
                    echo json_encode($result);
                    return;
                }

                //$data['model'] = new Devices;
                $data['connector'] = $connector_obj;
                $data['con_index'] = $POST['con_index'];
                
                if(isset($POST['con_obj']) && $POST['con_obj'] > 0){
                    $dev_con_obj = DevicesConnectors::model()->findByPk($POST['con_obj']);
                    if(!isset($dev_con_obj->id)){
                        $result['error'] = 'error: can not find dev_con_obj for id='.$POST['con_obj'];
                        echo json_encode($result);
                        return;
                    }
                    $data['con_obj'] = $dev_con_obj;
                }
                
                $result['html'] = $this->renderPartial('/devices/ajax/connector_details', $data, true);
            }
            else{
                $data['con_index'] = $POST['con_index'];
                $result['html'] = $this->renderPartial('/devices/ajax/connector_details_raw', $data, true);
            }
            
            echo json_encode($result);
        }
        
        
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Devices the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Devices::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Devices $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='devices-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
