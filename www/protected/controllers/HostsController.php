<?php

class HostsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','import'),
				'roles'=>array('operator'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','update','launch'),
                                'roles'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Hosts;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Hosts'])) {
                    
                        //print_r($_POST['Hosts']);
                        
                        $hostname = $_POST['Hosts']['hostname'];
                        //var_dump(Hosts::is_valid_domain_name($hostname));
                        
                        if(!isset($hostname) || Hosts::is_valid_domain_name($hostname) == FALSE){
                            
                            //--- wrong hostname format
                            $model->attributes=$_POST['Hosts'];
                            $this->render('create',array(
                                    'model'=>$model,
                                    'host_error'=>'Not valid domain name',
                            ));
                            
                            Yii::app()->end();
                        }
                        
                        //--- get IP and validate it
                        $ip=gethostbyname($hostname);
                        if(!filter_var($ip, FILTER_VALIDATE_IP))
                        {
                            //--- can not resolve hostname to IP
                            $model->attributes=$_POST['Hosts'];
                            $this->render('create',array(
                                    'model'=>$model,
                                    'host_error'=>'Can not resolve domain name to IP',
                            ));
                            
                            Yii::app()->end();
                        }
                        
                        //echo "IP: ".$ip;
                        //--- check device in our db
                        $criteria = new CDbCriteria();
                        $criteria->addCondition('ip = :ip');
                        $criteria->params = Array(':ip' => $ip,);

                        $device_model = Devices::model()->find($criteria);
                        
                        if(!isset($device_model->id) || $device_model->id < 1){
                            $device_model = new Devices();
                            $device_model->ip = $ip;
                            //-- edit later..
                            $device_model->port = 80;
                            $device_model->type = 'server';
                            $device_model->save();
                            $device_model->refresh();
                        }
                        
                        //die();
                    
			$model->attributes=$_POST['Hosts'];
                        
                        $model->id_device = $device_model->id;
                        
			if ($model->save()) {
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
                if (isset($_POST['Hosts'])) {
                    
                        //print_r($_POST['Hosts']);
                        
                        $hostname = $_POST['Hosts']['hostname'];
                        //var_dump(Hosts::is_valid_domain_name($hostname));
                        
                        if($model->hostname != $hostname){
                        
                            if(!isset($hostname) || Hosts::is_valid_domain_name($hostname) == FALSE){

                                //--- wrong hostname format
                                $model->attributes=$_POST['Hosts'];
                                $this->render('update',array(
                                        'model'=>$model,
                                        'host_error'=>'Not valid domain name',
                                ));

                                Yii::app()->end();
                            }

                            //--- get IP and validate it
                            $ip=gethostbyname($hostname);
                            if(!filter_var($ip, FILTER_VALIDATE_IP))
                            {
                                //--- can not resolve hostname to IP
                                $model->attributes=$_POST['Hosts'];
                                $this->render('update',array(
                                        'model'=>$model,
                                        'host_error'=>'Can not resolve domain name to IP',
                                ));

                                Yii::app()->end();
                            }

                            //echo "IP: ".$ip;
                            //--- check device in our db
                            $criteria = new CDbCriteria();
                            $criteria->addCondition('ip = :ip');
                            $criteria->params = Array(':ip' => $ip,);

                            $device_model = Devices::model()->find($criteria);

                            if(!isset($device_model->id) || $device_model->id < 1){
                                $device_model = new Devices();
                                $device_model->ip = $ip;
                                //-- edit later..
                                $device_model->port = 80;
                                $device_model->type = 'server';
                                $device_model->save();
                                $device_model->refresh();
                            }
                        
                            $model->id_device = $device_model->id;
                        //die();
                        }
			$model->attributes=$_POST['Hosts'];
                        
			if ($model->save()) {
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		} else {
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Hosts');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Hosts('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Hosts'])) {
			$model->attributes=$_GET['Hosts'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
        public function actionImport() {
            
            $model = new HostsListFile;
            //$file = CUploadedFile::getInstance($model,'csv_file');
            if(isset($_POST['HostsListFile']))
            {
                $model->attributes=$_POST['HostsListFile'];
                if(!empty($_FILES['HostsListFile']['tmp_name']['csv_file']))
                {
                    $file = CUploadedFile::getInstance($model,'csv_file');


                    $fp = fopen($file->tempName, 'r');
                    if($fp)
                    {
                        //  $line = fgetcsv($fp, 1000, ",");
                        //  print_r($line); exit;
                        $first_time = true;
                        
                     do {
                            if ($first_time == true) {
                                $first_time = false;
                                continue;
                            }
                            
                            //--- parse line
                            $url = trim($line[0]);
                            $connector_template = trim($line[1]);
                            
                            if(empty($url)){
                                $this->render('import',array(
                                        'model'=>$model,
                                        'host_error'=>'Empty url',
                                ));

                                Yii::app()->end();
                            }
                            
                            if(empty($connector_template)){
                                $this->render('import',array(
                                        'model'=>$model,
                                        'host_error'=>'Empty connector',
                                ));

                                Yii::app()->end();
                            }
                            
                            $criteria = new CDbCriteria();
                            $criteria->addCondition('name = :name');
                            $criteria->params = Array(':name' => $connector_template,);
                            $criteria->select = array('id');
                            
                            $connector_model = Connectors::model()->find($criteria);
                            
                            if(!isset($connector_model->id) || $connector_model->id < 1){
                                //--- can not resolve hostname to IP
                                $this->render('import',array(
                                        'model'=>$model,
                                        'host_error'=>'Wrong connector type',
                                ));

                                Yii::app()->end();
                            }
                            
                            //var_dump(parse_url($url));
                            //echo "</br>";
                            
                            $url_parts = parse_url($url);
                            
                            $protocol = $url_parts['scheme'];
                            $hostname = $url_parts['host'];
                            
                            //var_dump($hostname);die();
                            
                            $port = 80;
                            if(isset($url_parts['port'])){
                                $port = $url_parts['port'];
                            }
                            
                            if(isset($url_parts['user'])){
                                $user = $url_parts['user'];
                            }
                            
                            if(isset($url_parts['pass'])){
                                $pass = $url_parts['pass'];
                            }
                            
                            if(isset($url_parts['path'])){
                                $path = $url_parts['path'];
                            }
                            
                            if(isset($url_parts['query'])){
                                $query = $url_parts['query'];
                            }
                            
                            if(isset($url_parts['fragment'])){
                                $fragment = $url_parts['fragment'];
                            }
                            
                            
                            if(!isset($hostname) || Hosts::is_valid_domain_name($hostname) == FALSE){

                                //--- wrong hostname format
                                $this->render('import',array(
                                        'model'=>$model,
                                        'host_error'=>'Not valid domain name',
                                ));

                                Yii::app()->end();
                            }
                        
                            //--- get IP and validate it
                            $ip=gethostbyname($hostname);
                            if(!filter_var($ip, FILTER_VALIDATE_IP))
                            {
                                //--- can not resolve hostname to IP
                                $this->render('import',array(
                                        'model'=>$model,
                                        'host_error'=>'Can not resolve domain name to IP',
                                ));

                                Yii::app()->end();
                            }
                            
                            //---------- check if host with connector exists -------------------//
                            $criteria = new CDbCriteria();
                            $criteria->with = Array('hostsConnectors', 'hostsConnectors.idConnector');
                            $criteria->addCondition('hostname = :hostname AND hostsConnectors.port = :port AND idConnector.name = :connector');
                            $criteria->params = Array(':hostname' => $hostname, ':port' => $port, ':connector' => $connector_template);
                            $criteria->select = array('id');

                            $host = Hosts::model()->find($criteria);
                            
                            //var_dump($host);die();
                            
                            if(isset($host->id) && $host->id > 0){
                                //---------- check if device exists -------------------//
                                $criteria = new CDbCriteria();
                                $criteria->addCondition('ip = :ip AND port = :port');
                                $criteria->params = Array(':ip' => $ip, ':port' => $port);
                                $criteria->select = array('id');

                                $device = Devices::model()->find($criteria);
                                if(!isset($device->id) || $device->id < 1){
                                    $device = new Devices();
                                    $device->ip = $ip;
                                    $device->type = 'server';
                                    $device->save();
                                    $device->refresh();
                                }
                                
                                //-- host already exists
                                continue;
                            }
                            
                            $host = new Hosts();
                            $host->hostname = $hostname;

                            
                            //---------- check if device exists -------------------//
                            $criteria = new CDbCriteria();
                            $criteria->addCondition('ip = :ip AND port = :port');
                            $criteria->params = Array(':ip' => $ip, ':port' => $port);
                            $criteria->select = array('id');

                            $device = Devices::model()->find($criteria);
                            if(!isset($device->id) || $device->id < 1){
                                $device = new Devices();
                                $device->ip = $ip;
                                $device->type = 'server';
                                $device->save();
                                $device->refresh();
                            }
                            
                            $host->id_device = $device->id;
                            $host->save();
                            $host->refresh();
                            
                            //--- add connector
                            $connector = new HostsConnectors();
                            $connector->id_host = $host->id;
                            $connector->id_connector = $connector_model->id;
                            $connector->port = $port;
                            $connector->save();


                        }while( ($line = fgetcsv($fp, 1000, ";")) != FALSE);
                        
                        //$this->redirect('././index');

                    }
                    //    echo   $content = fread($fp, filesize($file->tempName));
                    //unlink($file->tempName);

                }

                $this->render('import',array(
                        'model'=>$model,
                        'import_log'=>'Import compleat',
                ));

                Yii::app()->end();
                
            }

            $this->render('import', array('model' => $model) );

        }
            

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Hosts the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Hosts::model()->findByPk($id);
		if ($model===null) {
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Hosts $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax']==='hosts-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}