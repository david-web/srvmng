<?php

class ConnectorsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create'),
				'roles'=>array('operator'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','update'),
                                'roles'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Connectors;
                
                $options_type = 'http';
                if(isset($_GET['options_type'])){
                    $options_type = $_GET['options_type'];
                }
                
                $data = array();
                $data['model'] = $model;
                
                switch ($options_type) {
                    case 'http':
                        $options_model = new ConnectorHttpParams;
                        
                        $form=$this->beginWidget('\TbActiveForm', array(
                                'id'=>'connectorHttpParams-form',
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        ));
                        
                        //$options_model;
                        $view = 'connector_http_params';
                        
                        $data['options_model']=$options_model;
                        $data['form']=$form;
                        
                        $data['connector_content'] = $this->renderPartial($view, $data, true);
                        break;
                    case 'backhttp':
                        $data['connector_content'] = "not implemented yet";
                        break;
                    case 'ssh':
                        $data['connector_content'] = "not implemented yet";
                        break;
                    case 'telnet':
                        $data['connector_content'] = "not implemented yet";
                        break;
                    default:
                       $data['connector_content'] = "not implemented yet";
                }
                

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Connectors'])) {
			$model->attributes=$_POST['Connectors'];
			if ($model->save()) {
				$model->refresh();
			}
                        
                        if(isset($_POST['ConnectorHttpParams'])){
                            
                            $ConnectorHttpParams_model = new ConnectorHttpParams;
                            $ConnectorHttpParams_model->attributes=$_POST['ConnectorHttpParams'];
                            $ConnectorHttpParams_model->connector_id = $model->id;
                            $ConnectorHttpParams_model->save();
                        }
                        
                        $this->redirect(array('view','id'=>$model->id));
		}

                //var_dump($data);die();
                
		$this->render('create',$data);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                $model=$this->loadModel($id);
                
                $options_type = $model->type;
                if(isset($_GET['options_type'])){
                    $options_type = $_GET['options_type'];
                }
                
                $data = array();
                $data['model'] = $model;
                
                switch ($options_type) {
                    case 'http':
                        $options_model = $model->connectorHttpParams;
                        
                        $form=$this->beginWidget('\TbActiveForm', array(
                                'id'=>'connectorHttpParams-form',
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        ));
                        
                        //$options_model;
                        $view = 'connector_http_params';
                        
                        $data['options_model']=$options_model;
                        $data['form']=$form;
                        
                        $data['connector_content'] = $this->renderPartial($view, $data, true);
                        
                        break;
                    case 'backhttp':
                        $data['connector_content'] = "not implemented yet";
                        break;
                    case 'ssh':
                        $data['connector_content'] = "not implemented yet";
                        break;
                    case 'telnet':
                        $data['connector_content'] = "not implemented yet";
                        break;
                    default:
                       $data['connector_content'] = "not implemented yet";
                }
                
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Connectors'])) {
			$model->attributes=$_POST['Connectors'];
			if ($model->save()) {
				$model->refresh();
			}
                        
                        if(isset($_POST['ConnectorHttpParams'])){
                            
                            //$ConnectorHttpParams_model = new ConnectorHttpParams;
                            
                            $ConnectorHttpParams_model = $model->connectorHttpParams;
                            $ConnectorHttpParams_model->attributes=$_POST['ConnectorHttpParams'];
                            $ConnectorHttpParams_model->admin_wrapper = $_POST['ConnectorHttpParams']['admin_wrapper'];
                            $ConnectorHttpParams_model->connector_id = $model->id;
                            $ConnectorHttpParams_model->save();
                        }
                        
                        $this->redirect(array('view','id'=>$model->id));
		}

                //var_dump($data);die();
                
		$this->render('update',$data);
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$model = $this->loadModel($id);
                        
                        $options_type = $model->type;

                        switch ($options_type) {
                            case 'http':
                                $options_model = $model->connectorHttpParams;
                                $options_model->delete();
                                
                                break;
                            case 'backhttp':
                                $data['connector_content'] = "not implemented yet";
                                break;
                            case 'ssh':
                                $data['connector_content'] = "not implemented yet";
                                break;
                            case 'telnet':
                                $data['connector_content'] = "not implemented yet";
                                break;
                            default:
                               $data['connector_content'] = "not implemented yet";
                        }
                        
                        $model->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		} else {
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Connectors');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Connectors('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Connectors'])) {
			$model->attributes=$_GET['Connectors'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Connectors the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Connectors::model()->findByPk($id);
		if ($model===null) {
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Connectors $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax']==='connectors-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}