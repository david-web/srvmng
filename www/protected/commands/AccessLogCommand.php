<?php

date_default_timezone_set('Europe/London');

class AccessLogCommand extends CConsoleCommand{

	private $log_dir = '../srvdownload';
        
        function multiRequest($data, $options = array()) {
 
                //print_r($options);
            
                $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
                $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
                $header[] = "Cache-Control: max-age=0";
                $header[] = "Connection: keep-alive";
                $header[] = "Keep-Alive: 300";
                $header[] = "Accept-Charset: UTF-8";
                $header[] = "Accept-Language: en-us,en;q=0.5";
                $header[] = "Pragma: "; // browsers keep this blank. 

                // array of curl handles
                $curly = array();
                // data to be returned
                $result = array();

                // multi handle
                $mh = curl_multi_init();

                // loop through $data and create curl handles
                // then add them to the multi-handle
                foreach ($data as $id => $d) {

                  $curly[$id] = curl_init();

                  $url = (is_array($d) && !empty($d['url'])) ? $d['url'] : $d;
                  curl_setopt($curly[$id], CURLOPT_URL,            $url);

                    //curl_setopt($curly[$id], CURLOPT_PROXY, "127.0.0.1:9150");
                    //curl_setopt($curly[$id], CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
                    /*
                    curl_setopt($curly[$id], CURLOPT_TIMEOUT_MS, 15000);
                    curl_setopt($curly[$id], CURLOPT_HEADER, TRUE);
                    curl_setopt($curly[$id], CURLOPT_RETURNTRANSFER, TRUE);
                    curl_setopt($curly[$id], CURLOPT_FOLLOWLOCATION, true);
                    curl_setopt($curly[$id], CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($curly[$id], CURLOPT_SSL_VERIFYHOST, false);
                    //curl_setopt($curly[$id], CURLOPT_NOBODY,1);
                    curl_setopt($curly[$id], CURLOPT_USERAGENT, 'Googlebot/2.1 (+http://www.google.com/bot.html)');
                    curl_setopt($curly[$id], CURLOPT_HTTPHEADER, $header);
                    curl_setopt($curly[$id], CURLOPT_REFERER, 'http://www.google.com');
                    curl_setopt($curly[$id], CURLOPT_ENCODING, 'gzip,deflate');
                    curl_setopt($curly[$id], CURLOPT_AUTOREFERER, true); 
                    */

                  // post?
                  if (is_array($d)) {
                    if (!empty($d['post'])) {
                      curl_setopt($curly[$id], CURLOPT_POST,       1);
                      curl_setopt($curly[$id], CURLOPT_POSTFIELDS, $d['post']);
                    }
                  }

                  // extra options?
                  if (!empty($options)) {
                    curl_setopt_array($curly[$id], $options);
                  }

                  curl_multi_add_handle($mh, $curly[$id]);
                }

                // execute the handles
                $running = null;
                do {
                  curl_multi_exec($mh, $running);
                } while($running > 0);


                // get content and remove handles
                foreach($curly as $id => $c) {
                  $result[$id] = curl_multi_getcontent($c);
                  curl_multi_remove_handle($mh, $c);
                }

                // all done
                curl_multi_close($mh);

                return $result;
        }

	public function actionIndex(){
                echo "It works";
	}
        
       
}
