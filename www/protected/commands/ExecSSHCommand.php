<?php

date_default_timezone_set('Europe/London');

class ExecSSHCommand extends CConsoleCommand{

	private $log_dir = 'runtime';
        
        private function log($str){
            $date = date('d-m-Y H:i:s a', time());
            $log_str = $date." ".$str.PHP_EOL;
            file_put_contents($this->log_dir.'/ExecSSH.log', $log_str, FILE_APPEND);
        }
        

        public function actionExecOld($ip=null,$login=null,$pass=null,$cmd=null){
            /*
            $ip='192.168.56.111';
            $login='test2';
            $pass='2test';
            $cmd='echo check1235';
            */
            $str = "Exec ssh ip=".$ip." login=".$login." pass=".$pass." cmd=".$cmd." result: ";
            
            // http://phpseclib.sourceforge.net/ssh/auth.html
            
            $ssh = Yii::app()->phpseclib->createSSH2($ip);
            //var_dump($ssh);die();
            
            if($ssh == false || $ssh->fsock == false){
                $str1 = "connection failed";
                $str.= $str1;
                $this->log($str);
                return($str1);
            }else{
                if (!$ssh->login($login, $pass)) {
                    $str1 = 'login failed';
                    $str.= $str1;
                    $this->log($str);
                    return($str1);
                }
                
                $result = $ssh->exec($cmd);
                $str .= $result;
                $this->log($str);
                return($result);
            }
            
        }
        
        public function actionExec($params){
            /*
            $ip='192.168.56.111';
            $login='test2';
            $pass='2test';
            $cmd='echo check1235';
            */
            
            extract($params, EXTR_OVERWRITE);
            $str = "Exec ssh ip=".$ip." login=".$login." pass=".$pass." cmd=".$cmd." result: ";
            
            // http://phpseclib.sourceforge.net/ssh/auth.html
            
            $ssh = Yii::app()->phpseclib->createSSH2($ip);
            //var_dump($ssh);die();
            
            if($ssh == false || $ssh->fsock == false){
                $str1 = "connection failed";
                $str.= $str1;
                $this->log($str);
                return($str1);
            }else{
                if (!$ssh->login($login, $pass)) {
                    $str1 = 'login failed';
                    $str.= $str1;
                    $this->log($str);
                    return($str1);
                }
                
                $result = $ssh->exec($cmd);
                $str .= $result;
                $this->log($str);
                return($result);
            }
            
        }
        
}
