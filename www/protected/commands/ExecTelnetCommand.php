<?php

date_default_timezone_set('Europe/London');
//use TelnetClient\TelnetClient;

class ExecTelnetCommand extends CConsoleCommand{

	private $log_dir = 'runtime';
        
        private function log($str){
            $date = date('d-m-Y H:i:s a', time());
            $log_str = $date." ".$str.PHP_EOL;
            file_put_contents($this->log_dir.'/ExecTelnet.log', $log_str, FILE_APPEND);
        }
        

        public function actionExec($params){
            /*
            $ip='192.168.56.103';
            $login='test3';
            $pass='2test';
            $cmd='echo "it works"';
            */
            
            extract($params, EXTR_OVERWRITE);
            $str = "Exec ssh ip=".$ip." login=".$login." pass=".$pass." cmd=".$cmd." result: ";
            
            if(!isset($port))
                $port=23;
            
            $telnet = new TelnetClient($ip, $port);
            
            $con = $telnet->connect();
            if($con == false){
                $str1 = "connection failed";
                $str.= $str1;
                $this->log($str);
                return($str1);
            }

            $telnet->setPrompt('$'); //setRegexPrompt() to use a regex
            //$telnet->setPruneCtrlSeq(true); //Enable this to filter out ANSI control/escape sequences
            
            if(isset($login) && !empty($login) && isset($pass)){
                $login_status = $telnet->login($login, $pass);
                    if($login_status == false){
                    $str1 = 'login failed';
                    $str.= $str1;
                    $this->log($str);
                    return($str1);
                }
            }

            $telnet->exec('unset HISTFILE');
            $cmdResult = $telnet->exec($cmd);
            //var_dump($cmdResult);
            
            $telnet->disconnect();
            
            /*
            $cmdResult[0] - entered cmd
            $cmdResult[1] - can be new line (empty)
            $cmdResult[2] - (n-1)] - output
            $cmdResult[n] - new line (empty)
            */
            
            //-- clear the output
            // The following lines will remove values from the first two indexes.
            unset($cmdResult[0]);
            if(empty($cmdResult[1])){
                unset($cmdResult[1]);
            }
            // This line will re-set the indexes (the above just nullifies the values...) and make a     new array without the original first two slots.
            $cmdResult = array_values($cmdResult);
            array_pop($cmdResult);
            
            //-- join result to string
            $result = implode(PHP_EOL, $cmdResult);
            //var_dump($result);die();
            
            $str .= $result;
            $this->log($str);
            return($result);
            
        }
        
}
