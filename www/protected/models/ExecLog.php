<?php

/**
 * This is the model class for table "exec_log".
 *
 * The followings are the available columns in table 'exec_log':
 * @property string $id
 * @property string $id_command
 * @property string $id_obj
 * @property string $obj_type
 * @property integer $id_connector
 * @property string $result
 * @property string $created
 *
 * The followings are the available model relations:
 * @property Connectors $idConnector
 * @property Commands $idCommand
 */
class ExecLog extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'exec_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('', 'required'),
			array('id_connector', 'numerical', 'integerOnly'=>true),
			array('id_command, id_obj', 'length', 'max'=>20),
			array('obj_type', 'length', 'max'=>1),
			array('result', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_command, id_obj, obj_type, id_connector, result, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idConnector' => array(self::BELONGS_TO, 'Connectors', 'id_connector'),
			'idCommand' => array(self::BELONGS_TO, 'Commands', 'id_command'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_command' => 'Command',
			'id_obj' => 'Id Obj',
			'obj_type' => 'Obj Type',
			'id_connector' => 'Connector',
			'result' => 'REsult',
			'created' => 'Timestamp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('id_command',$this->id_command,true);
		$criteria->compare('id_obj',$this->id_obj,true);
		$criteria->compare('obj_type',$this->obj_type,true);
		$criteria->compare('id_connector',$this->id_connector);
		$criteria->compare('result',$this->result,true);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort'=>array("defaultOrder"=>"id DESC")
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ExecLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
