<?php

/**
 * This is the model class for table "devices".
 *
 * The followings are the available columns in table 'devices':
 * @property string $id
 * @property string $ip
 * @property integer $port
 * @property string $type
 * @property string $created
 * @property string $is_online
 * @property string $last_checked
 * @property string $os
 * @property string $arch
 * @property string $php_info
 * @property string $comment
 *
 * The followings are the available model relations:
 * @property ConnectorSshParams[] $connectorSshParams
 * @property ConnectorTelnetParams[] $connectorTelnetParams
 * @property DevicesConnectors[] $devicesConnectors
 * @property DevicesListsVal[] $devicesListsVals
 * @property DevicesTags[] $devicesTags
 */
class Devices extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
    
        public $connector; 
    
	public function tableName()
	{
		return 'devices';
	}
        
        public  $type_options = array(
                    'server' => 'Server',
                    'pc' => 'PC',
                    'router' => 'Router',
                    'cctv' => 'CCTV'
                );
        
        public  $online_options = array(
                    'Y' => 'Online',
                    'N' => 'Offline',
                );
        
        public  $arch_options = array(
                    '' => 'Select Arch',
                    'arm' => 'ARM',
                    'x64' => 'x64',
                    'x86' => 'x86',
                    'mipsbe' => 'MIPSbe',
                    'mipsle' => 'MIPSle',
                    'powerpc' => 'POWERPC',
                );

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ip,type', 'required'),
			array('port', 'numerical', 'integerOnly'=>true),
			array('ip', 'length', 'max'=>30),
			array('type', 'length', 'max'=>6),
			array('is_online', 'length', 'max'=>1),
			array('arch', 'length', 'max'=>7),
			array('last_checked, os, php_info, comment', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, ip, port, type, created, is_online, last_checked, os, arch, php_info, comment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'connectorSshParams' => array(self::HAS_MANY, 'ConnectorSshParams', 'id_device'),
			'connectorTelnetParams' => array(self::HAS_MANY, 'ConnectorTelnetParams', 'id_device'),
			'devicesConnectors' => array(self::HAS_MANY, 'DevicesConnectors', 'id_device'),
			'devicesListsVals' => array(self::HAS_MANY, 'DevicesListsVal', 'id_device'),
			'devicesTags' => array(self::HAS_MANY, 'DevicesTags', 'id_device'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Device ID',
			'ip' => 'IP address/Connectors',
                        'connector' => 'Connector',
                        'ip_raw' => 'IP address',
			'port' => 'Port',
			'type' => 'Type',
			'created' => 'Created',
			'is_online' => 'Online',
			'last_checked' => 'Last Checked',
			'os' => 'OS',
			'arch' => 'Arch',
			'php_info' => 'PHP_INFO',
			'comment' => 'Comment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('port',$this->port);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('is_online',$this->is_online,true);
		$criteria->compare('last_checked',$this->last_checked,true);
		$criteria->compare('os',$this->os,true);
		$criteria->compare('arch',$this->arch,true);
		$criteria->compare('php_info',$this->php_info,true);
		$criteria->compare('comment',$this->comment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Devices the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getDeviceConnectorsText()
        {
            
            //$device_connectors=DevicesConnectors::model()->findByAttributes(array('id_device'=>$id_device));
            
            $device_connectors=$this->devicesConnectors;
            
            if(empty($device_connectors)){
                return $this->ip;
            }
            
            $connectors = '<div class="list-group" style="margin-bottom: 0px;">';
            $connectors .= '<a class="list-group-item active">'.$this->ip.'</a>';
            foreach($device_connectors as $connector){
                $connectors .= '<a class="list-group-item">'.
                        '<span class="badge">'.$connector->port.'</span>'.
                        $connector->idConnector->name.
                        '</a>';
            }
            $connectors .= '</div>';
            
            
            return $connectors;
        }
        
        public function getDeviceConnector($id_device=null)
        {
            
            //$device_connectors=DevicesConnectors::model()->findByAttributes(array('id_device'=>$id_device));

            if(isset($id_device) && $id_device >0){
                $device= Devices::model()->findByPk($id_device);
                if(!isset($device->id) || $device->id < 1){
                    echo "error: can not find device obj for id=".$id_device;
                    die();
                }
                $device_connectors=$device->devicesConnectors;
            }
            else{
                $device_connectors=$this->devicesConnectors;
            }
            
            if(empty($device_connectors)){
                return null;
            }
            
            foreach($device_connectors as $connector){
                if($connector->idConnector->type == 'ssh'){
                    //-- highest wheit connector
                    return $connector;
                }
                elseif($connector->idConnector->type == 'telnet'){
                    //-- high wheit connector
                    return $connector;
                }
            }
            
            return $device_connectors[0];
        }
        
        public function updateOnline($id_device=null){
                
            if(isset($id_device) && $id_device >0){
                $device = Devices::model()->findByPk($id_device);
                if(!isset($device->id) || $device->id < 1){
                    echo "error: can not find device obj for id=".$id_device;
                    die();
                }
                $device->is_online = 'Y';
                $device->last_checked = date("Y-m-d H:i:s");
                $device->save();
            }
            else{
                $this->is_online = 'Y';
                $this->last_checked = date("Y-m-d H:i:s");
                $this->save();
            }

            return;
	}
        
        public function updateOffline($id_device=null){
                
            if(isset($id_device) && $id_device >0){
                $device = Devices::model()->findByPk($id_device);
                if(!isset($device->id) || $device->id < 1){
                    echo "error: can not find device obj for id=".$id_device;
                    die();
                }
                $device->is_online = 'N';
                $device->last_checked = date("Y-m-d H:i:s");
                $device->save();
            }
            else{
                $this->is_online = 'N';
                $this->last_checked = date("Y-m-d H:i:s");
                $this->save();
            }

            return;
	}
        
        protected function afterFind(){

                //$this->type = $this->type_options[$this->type];
                //$this->is_online = $this->online_options[$this->is_online];
                
                if($this->arch != '')
                    $this->arch = $this->arch_options[$this->arch];
            
		return parent::afterFind();
	}
}
