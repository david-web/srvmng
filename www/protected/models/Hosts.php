<?php

/**
 * This is the model class for table "hosts".
 *
 * The followings are the available columns in table 'hosts':
 * @property string $id
 * @property string $hostname
 * @property string $id_device
 * @property string $created
 * @property string $is_online
 * @property string $last_checked
 * @property string $php_info
 *
 * The followings are the available model relations:
 * @property HostsConnectors[] $hostsConnectors
 * @property HostsListVal[] $hostsListVals
 * @property HostsTags[] $hostsTags
 */
class Hosts extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'hosts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('hostname,', 'required'),
			array('id_device', 'length', 'max'=>20),
			array('is_online', 'length', 'max'=>1),
			array('last_checked, php_info', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, hostname, id_device, created, is_online, last_checked, php_info', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'hostsConnectors' => array(self::HAS_MANY, 'HostsConnectors', 'id_host'),
			'hostsListVals' => array(self::HAS_MANY, 'HostsListVal', 'id_host'),
			'hostsTags' => array(self::HAS_MANY, 'HostsTags', 'id_host'),
                        'devices_table' => array(self::HAS_ONE, 'Devices', 'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Host ID',
			'hostname' => 'Hostname/Connectors',
			'id_device' => 'Device',
			'created' => 'Created',
			'is_online' => 'Online',
			'last_checked' => 'Last Checked',
			'php_info' => 'PHP_INFO',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('hostname',$this->hostname,true);
		$criteria->compare('id_device',$this->id_device,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('is_online',$this->is_online,true);
		$criteria->compare('last_checked',$this->last_checked,true);
		$criteria->compare('php_info',$this->php_info,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function is_valid_domain_name($domain_name)
        {
            return (preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $domain_name) //valid chars check
                    && preg_match("/^.{1,253}$/", $domain_name) //overall length check
                    && preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $domain_name)   ); //length of each label
        }
        
        public function getHostsConnectorsText(){
                        
            $host_connectors=$this->hostsConnectors;
            
            //var_dump($device_connectors);die();
            
            if(empty($host_connectors)){
                return $this->hostname;
            }
            
            $connectors = '<div class="list-group" style="margin-bottom: 0px;">';
            $connectors .= '<a class="list-group-item active">'.$this->hostname.'</a>';
            foreach($host_connectors as $connector){
                $connectors .= '<a class="list-group-item">'.
                        '<span class="badge">'.$connector->port.'</span>'.
                        $connector->idConnector->name.
                        '</a>';
            }
            $connectors .= '</div>';
            
            
            return $connectors;
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Hosts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
