<?php

/**
 * This is the model class for table "commands".
 *
 * The followings are the available columns in table 'commands':
 * @property string $id
 * @property string $name
 * @property string $type
 * @property string $param
 * @property string $created
 * @property string $created_by
 * @property string $updated
 * @property string $updated_by
 *
 * The followings are the available model relations:
 * @property CommandConnectors[] $commandConnectors
 * @property CommandLists[] $commandLists
 * @property Users $createdBy
 * @property Users $updatedBy
 */
class Commands extends CActiveRecord
{
	
        public  $type_options = array(
            'D' => 'Download & Execute',
            'E' => 'Execute',
        );
    
        /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'commands';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, param,', 'required'),
			array('name', 'length', 'max'=>120),
			array('type', 'length', 'max'=>1),
			array('created_by, updated_by', 'length', 'max'=>20),
			array('updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, type, param, created, created_by, updated, updated_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'commandConnectors' => array(self::HAS_MANY, 'CommandConnectors', 'id_command'),
                        'commandConnector' => array(self::HAS_ONE, 'CommandConnectors', 'id_command'), //-- tmp get top 1 connector
			'commandLists' => array(self::HAS_MANY, 'CommandLists', 'id_command'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Command ID',
			'name' => 'Name',
			'type' => 'Type',
			'param' => 'Param',
			'created' => 'Created',
			'created_by' => 'Created By',
			'updated' => 'Updated',
			'updated_by' => 'Updated By',
                        'select_conn' => 'Select connectors',
                        'run_as_adm' => 'Run as Admin',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('param',$this->param,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('updated_by',$this->updated_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Commands the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        protected function beforeSave(){

		if($this->created_by < 1){
                    $this->created_by = Yii::app()->user->id;
                }
                else{
                    $this->updated = date("Y-m-d H:i:s");
                    $this->updatedBy = Yii::app()->user->id;
                }
                
		return parent::beforeSave();
        }
        
        protected function afterFind(){
            
                /*
                $this->type = $this->type_options[$this->type];

                $this->created_by = $this->createdBy->username;
                
                if($this->updated_by > 0)
                    $this->updated_by = $this->createdBy->username;
                */
            
		return parent::afterFind();
	}
        
}
