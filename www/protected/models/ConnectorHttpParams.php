<?php

/**
 * This is the model class for table "connector_http_params".
 *
 * The followings are the available columns in table 'connector_http_params':
 * @property integer $id
 * @property integer $connector_id
 * @property string $proto
 * @property string $method
 * @property string $path
 * @property string $post_data
 * @property string $cookie
 * @property string $headers
 *
 * The followings are the available model relations:
 * @property Connectors $connector
 */
class ConnectorHttpParams extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'connector_http_params';
	}

        public  $proto_options = array(
            'http' => 'HTTP',
            'https' => 'HTTPS',
        );
        
        public  $method_options = array(
            'get' => 'GET',
            'post' => 'POST',
            'cookie' => 'COOKIE',
            'header' => 'HEADER',
        );
        
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('path', 'required'),
			array('connector_id', 'numerical', 'integerOnly'=>true),
			array('proto', 'length', 'max'=>5),
			array('method', 'length', 'max'=>6),
			array('post_data, cookie, headers admin_wrapper', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, connector_id, proto, method, path, post_data, cookie, headers, admin_wrapper', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'connector' => array(self::BELONGS_TO, 'Connectors', 'connector_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'connector_id' => 'Connector',
			'proto' => 'Protocol',
			'method' => 'Method',
			'path' => 'Path',
			'post_data' => 'Params',
			'cookie' => 'Cookie',
			'headers' => 'Headers',
                        'admin_wrapper' => 'Admin (root) wrapper',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('connector_id',$this->connector_id);
		$criteria->compare('proto',$this->proto,true);
		$criteria->compare('method',$this->method,true);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('post_data',$this->post_data,true);
		$criteria->compare('cookie',$this->cookie,true);
		$criteria->compare('headers',$this->headers,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ConnectorHttpParams the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        protected function afterFind(){
            
                //$this->proto = $this->proto_options[$this->proto];
                //$this->method = $this->method_options[$this->method];

		return parent::afterFind();
	}
}
