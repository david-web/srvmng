<?php
/* @var $this ExecLogController */
/* @var $model ExecLog */
?>

<?php
$this->breadcrumbs=array(
	'Exec Logs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ExecLog', 'url'=>array('index')),
	array('label'=>'Manage ExecLog', 'url'=>array('admin')),
);
?>

<h1>Create ExecLog</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>