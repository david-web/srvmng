<?php
/* @var $this ExecLogController */
/* @var $data ExecLog */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_command')); ?>:</b>
	<?php echo CHtml::encode($data->id_command); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_obj')); ?>:</b>
	<?php echo CHtml::encode($data->id_obj); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('obj_type')); ?>:</b>
	<?php echo CHtml::encode($data->obj_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_connector')); ?>:</b>
	<?php echo CHtml::encode($data->id_connector); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('result')); ?>:</b>
	<?php echo CHtml::encode($data->result); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />


</div>