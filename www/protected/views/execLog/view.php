<?php
/* @var $this ExecLogController */
/* @var $model ExecLog */
?>

<?php
$this->breadcrumbs=array(
	'Exec Logs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ExecLog', 'url'=>array('index')),
	array('label'=>'Create ExecLog', 'url'=>array('create')),
	array('label'=>'Update ExecLog', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ExecLog', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ExecLog', 'url'=>array('admin')),
);
?>

<h1>View ExecLog #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id',
		'id_command',
		'id_obj',
		'obj_type',
		'id_connector',
		'result',
		'created',
	),
)); ?>