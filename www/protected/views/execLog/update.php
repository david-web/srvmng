<?php
/* @var $this ExecLogController */
/* @var $model ExecLog */
?>

<?php
$this->breadcrumbs=array(
	'Exec Logs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ExecLog', 'url'=>array('index')),
	array('label'=>'Create ExecLog', 'url'=>array('create')),
	array('label'=>'View ExecLog', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ExecLog', 'url'=>array('admin')),
);
?>

    <h1>Update ExecLog <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>