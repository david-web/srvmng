<?php
/* @var $this ExecLogController */
/* @var $model ExecLog */


$this->breadcrumbs=array(
	'Exec Logs'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ExecLog', 'url'=>array('index')),
	array('label'=>'Create ExecLog', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#exec-log-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Exec Logs</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('\TbGridView',array(
	'id'=>'exec-log-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		//'id_command',
                array('name'=>'id_command','type'=>'raw','value'=>'$data->idCommand->name'),
		'id_obj',
		//'obj_type',
                array('name'=>'obj_type','type'=>'raw','value'=>'($data->obj_type == \'D\' ? \'<span class="label label-success">Device</span>\' : \'<span class="label label-default">Host</span>\')'),
		//'id_connector',
                array('name'=>'id_connector','type'=>'raw','value'=>'$data->idConnector->name'),
		'result',
		/*
		'created',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>