<?php
/* @var $this ExecLogController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Exec Logs',
);

$this->menu=array(
	array('label'=>'Create ExecLog','url'=>array('create')),
	array('label'=>'Manage ExecLog','url'=>array('admin')),
);
?>

<h1>Exec Logs</h1>

<?php $this->widget('\TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>