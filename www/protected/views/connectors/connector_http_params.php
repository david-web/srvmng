<div class="form">
        
        <legend>HTTP connector options</legend>

        <?php 
                $selected = 'http';
                if(isset($options_model->proto) && !empty($options_model->proto)){
                    $selected = $options_model->proto;
                }
                echo TbHtml::label('Protocol', 'text');
                echo $form->dropDownList($options_model,'proto', ConnectorHttpParams::model()->proto_options, array('selected' => $selected));
                
                
                $selected = 'post';
                if(isset($options_model->method) && !empty($options_model->method)){
                    $selected = $options_model->method;
                }
                echo TbHtml::label('Inject method', 'text');
                echo $form->dropDownList($options_model,'method', ConnectorHttpParams::model()->method_options, array('selected' => $selected));

                if(!empty($options_model->path)){
                    //$options_model->path = $options_model::Filter($model->path);
                }
                echo $form->textFieldControlGroup($options_model,'path', array('span'=>5,'maxlength'=>300));
                
                echo TbHtml::label('POST request data', 'text');
                if(!empty($options_model->post_data)){
                    //$options_model->post_data = $options_model::Filter($model->post_data);
                }
                echo $form->textArea($options_model,'post_data', array('rows' => 5));
                
                echo TbHtml::label('COOKIE request data', 'text');
                if(!empty($options_model->cookie)){
                    //$options_model->cookie = $options_model::Filter($model->cookie);
                }
                echo $form->textArea($options_model,'cookie', array('rows' => 3));
                
                echo TbHtml::label('HEADERS request data', 'text');
                if(!empty($options_model->headers)){
                    //$options_model->headers = $options_model::Filter($model->headers);
                }
                echo $form->textArea($options_model,'headers', array('rows' => 3));
                
                echo TbHtml::label('Admin (root) wrapper', 'text');
                if(!empty($options_model->admin_wrapper)){
                    //$options_model->headers = $options_model::Filter($model->headers);
                }
                echo $form->textArea($options_model,'admin_wrapper', array('rows' => 3));
        ?> 
    
    

</div>