<?php
/* @var $this ConnectorsController */
/* @var $model Connectors */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('\TbActiveForm', array(
	'id'=>'connectors-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldControlGroup($model,'name',array('span'=>5,'maxlength'=>120)); ?>

            <?php 
                    //echo $form->textFieldControlGroup($model,'type',array('span'=>5,'maxlength'=>8)); 
            
                    $selected = 'http';
                    if(isset($model->type) && !empty($model->type)){
                        $selected = $model->type;
                    }
                    if(isset($_GET['options_type'])){
                        $selected = htmlspecialchars($_GET['options_type']);
                        $model->type = $selected;
                    }
                    
                    echo TbHtml::label('Connector type', 'text');
                    echo $form->dropDownList($model,'type', Connectors::model()->type_options, array('selected' => $selected));
            ?>        
                    <?php echo "<br>"; ?>
                    
            <?php        
                    //var_dump($connector_content);die();
                    $this->widget('bootstrap.widgets.TbTabs', array(
                        'tabs' => array(
                            array('label' => 'Advanced Options', 'content' => $connector_content, 'active' => true),

                        ),
                    ));
                    
            ?>

        <div class="form-actions">  
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->