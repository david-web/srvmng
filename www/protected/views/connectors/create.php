<?php
/* @var $this ConnectorsController */
/* @var $model Connectors */
?>

<?php Yii::app()->clientScript->registerScriptFile("/js/connectors.js", CClientScript::POS_END); ?> 

<?php
$this->breadcrumbs=array(
	'Connectors'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Connectors', 'url'=>array('index')),
	array('label'=>'Manage Connectors', 'url'=>array('admin')),
);
?>

<h1>Create Connectors</h1>

<?php 
    $data = array();
    
    $data['model'] = $model;
    
    if(isset($connector_content))
        $data['connector_content'] = $connector_content;
        
    $this->renderPartial('_form', $data); 
?>