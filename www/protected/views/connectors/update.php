<?php
/* @var $this ConnectorsController */
/* @var $model Connectors */
?>

<?php Yii::app()->clientScript->registerScriptFile("/js/connectors.js", CClientScript::POS_END); ?>  

<?php
$this->breadcrumbs=array(
	'Connectors'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Connectors', 'url'=>array('index')),
	array('label'=>'Create Connectors', 'url'=>array('create')),
	array('label'=>'View Connectors', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Connectors', 'url'=>array('admin')),
);
?>

    <h1>Update Connectors <?php echo $model->id; ?></h1>

<?php 
    $data = array();
    $data['model'] = $model;
    
    if(isset($connector_content))
        $data['connector_content'] = $connector_content;
        
    $this->renderPartial('_form', $data); 
?>