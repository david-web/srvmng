<?php
/* @var $this ConnectorsController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Connectors',
);

$this->menu=array(
	array('label'=>'Create Connectors','url'=>array('create')),
	array('label'=>'Manage Connectors','url'=>array('admin')),
);
?>

<h1>Connectors</h1>

<?php $this->widget('\TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>