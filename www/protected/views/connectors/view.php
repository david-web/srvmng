<?php
/* @var $this ConnectorsController */
/* @var $model Connectors */
?>

<?php
$this->breadcrumbs=array(
	'Connectors'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Connectors', 'url'=>array('index')),
	array('label'=>'Create Connectors', 'url'=>array('create')),
	array('label'=>'Update Connectors', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Connectors', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Connectors', 'url'=>array('admin')),
);
?>

<h1>View Connectors #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id',
		'name',
		'type',
	),
)); ?>