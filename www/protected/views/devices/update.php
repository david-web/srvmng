<?php
/* @var $this DevicesController */
/* @var $model Devices */
?>

<?php Yii::app()->clientScript->registerScriptFile("/js/devices.js", CClientScript::POS_END); ?> 

<?php
$this->breadcrumbs=array(
	'Devices'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Devices', 'url'=>array('index')),
	array('label'=>'Create Devices', 'url'=>array('create')),
	array('label'=>'View Devices', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Devices', 'url'=>array('admin')),
);
?>

<h1>Update Devices <?php echo $model->id; ?></h1>

<?php 

$options = array(
    'model'=>$model,
    'dev_connectors'=>$dev_connectors,
    );
if(isset($con_obj)){
    $options['con_obj']=$con_obj;
}

$this->renderPartial('_form', $options); 
    
?>