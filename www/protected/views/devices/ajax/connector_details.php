

<?php 
        echo TbHtml::label('Connector type', 'text');

        /*
        $selected = 'N';
        if(isset($model->is_online) && !empty($model->is_online)){
            $selected = $model->is_online;
        }
        */
        //var_dump($con_obj->id);die();
        
        $options = array('id' => 'Devices_connector',
            'class' => 'get_con_detail',
            'data-id' => $con_index
            );
        
        if(isset($con_obj->id) && $con_obj->id > 0){
            $options['data-con_obj'] =  $con_obj->id;
            echo CHtml::hiddenField("connector[$con_index][con_obj]" , $con_obj->id);
        }

        echo TbHtml::dropDownList("connector[$con_index][id]",$connector->id, Connectors::model()->getConnectors(),
                $options);


        switch ($connector->type) {
            case 'http':
                $port=80;
                break;
            case 'telnet':
                $port=23;
                echo TbHtml::label('Login','text');
                echo TbHtml::textField("connector[$con_index][login]", '', array('placeholder' => 'username'));
                echo TbHtml::label('Password','text');
                echo TbHtml::textField("connector[$con_index][pass]", '', array('placeholder' => 'password'));
                break;
            case 'ssh':
                $port=22;
                echo TbHtml::label('Login','text');
                echo TbHtml::textField("connector[$con_index][login]", '', array('placeholder' => 'username'));
                echo TbHtml::label('Password','text');
                echo TbHtml::textField("connector[$con_index][pass]", '', array('placeholder' => 'password'));
                break;
        }

        echo TbHtml::label('Port','text');
        echo TbHtml::textField("connector[$con_index][port]", $port, array('placeholder' => 'Port number'));

        $options = array('color' => TbHtml::BUTTON_COLOR_DANGER,
            'size' => TbHtml::BUTTON_SIZE_MINI,
            'id' => 'remove_connector',
            'class' => 'remove_connector',
            'onclick' =>'remove_connector1(this);',
        );
        
        if(isset($con_obj->id)){
            $options['data-con_obj'] = $con_obj->id;
        }
        
        echo TbHtml::button('Remove connector', $options);
        echo "<p></p>";
?> 

