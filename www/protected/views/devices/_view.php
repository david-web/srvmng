<?php
/* @var $this DevicesController */
/* @var $data Devices */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ip')); ?>:</b>
	<?php echo CHtml::encode($data->ip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('port')); ?>:</b>
	<?php echo CHtml::encode($data->port); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_online')); ?>:</b>
	<?php echo CHtml::encode($data->is_online); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_checked')); ?>:</b>
	<?php echo CHtml::encode($data->last_checked); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('os')); ?>:</b>
	<?php echo CHtml::encode($data->os); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('arch')); ?>:</b>
	<?php echo CHtml::encode($data->arch); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('php_info')); ?>:</b>
	<?php echo CHtml::encode($data->php_info); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comment')); ?>:</b>
	<?php echo CHtml::encode($data->comment); ?>
	<br />

	*/ ?>

</div>