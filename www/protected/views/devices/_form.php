<?php
/* @var $this DevicesController */
/* @var $model Devices */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('\TbActiveForm', array(
	'id'=>'devices-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'ip_raw'); ?>
		<?php echo $form->textField($model,'ip',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'ip'); ?>
	</div>
        
        <div id="connectors">
            <!--
            <div class="row" id="connector_div">
                    
            </div>
            -->
            <?php
            
            if(isset($dev_connectors) && count($dev_connectors) > 0){
                $i=1;
                foreach($dev_connectors as $dev_connector){
                    ?>
                    <div class="row" id="connector_div" data-id="<?php echo $i; ?>">
                    <?php
                    echo TbHtml::label('Connector type', 'text');
                
                    /*
                    $selected = 'N';
                    if(isset($model->is_online) && !empty($model->is_online)){
                        $selected = $model->is_online;
                    }
                    */
                    echo CHtml::hiddenField("connector[$i][con_obj]" , $dev_connector->id);
                    
                    echo TbHtml::dropDownList("connector[$i][id]", $dev_connector->id_connector, Connectors::model()->getConnectors(),
                            array('id' => 'Devices_connector',
                                'class' => 'get_con_detail',
                                'data-id' => $i,
                                'data-con_obj' => $dev_connector->id,
                                ));
                    
                    switch ($dev_connector->idConnector->type) {
                        case 'telnet':
                            
                            $con_telnet_param_obj = ConnectorTelnetParams::model()->findByAttributes(Array('id_device'=>$dev_connector->id_device));
                            
                            echo TbHtml::label('Login','text');
                            echo TbHtml::textField("connector[$i][login]", $con_telnet_param_obj->login, array('placeholder' => 'username'));
                            echo TbHtml::label('Password','text');
                            echo TbHtml::textField("connector[$i][pass]", $con_telnet_param_obj->pass, array('placeholder' => 'password'));
                            break;
                        case 'ssh':
                            
                            $con_ssh_param_obj = ConnectorSshParams::model()->findByAttributes(Array('id_device'=>$dev_connector->id_device));
                            
                            echo TbHtml::label('Login','text');
                            echo TbHtml::textField("connector[$i][login]", $con_ssh_param_obj->login, array('placeholder' => 'username'));
                            echo TbHtml::label('Password','text');
                            echo TbHtml::textField("connector[$i][pass]", $con_ssh_param_obj->pass, array('placeholder' => 'password'));
                            break;
                    }

                    echo TbHtml::label('port','text');
                    echo TbHtml::textField("connector[$i][port]", $dev_connector->port, array('placeholder' => 'Port number'));

                    echo TbHtml::button('Remove connector', 
                            array('color' => TbHtml::BUTTON_COLOR_DANGER,
                                'size' => TbHtml::BUTTON_SIZE_MINI,
                                'id' => 'remove_connector',
                                'class' => 'remove_connector',
                                'onclick' =>'remove_connector1(this);',
                                'data-con_obj' => $dev_connector->id,
                            ));
                    echo "<p></p>";
                    ++$i;
                    ?>
                    </div>    
                    <?php
                }
            }
            
            ?>
            
        </div>
        
        <div class="row">
                <?php echo TbHtml::button('Add connector', 
                        array('color' => TbHtml::BUTTON_COLOR_PRIMARY,
                            'size' => TbHtml::BUTTON_SIZE_MINI,
                            'id' => 'add_connector')); ?>
        </div>
        
        <!--
	<div class="row">
		<?php //echo $form->labelEx($model,'port'); ?>
		<?php //echo $form->textField($model,'port'); ?>
		<?php //echo $form->error($model,'port'); ?>
	</div>
        -->
        
	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
                <?php 
                        $selected = 'server';
                        if(isset($model->type) && !empty($model->type)){
                            $selected = $model->type;
                        }
                
                        echo $form->dropDownList($model,'type', Devices::model()->type_options, array('selected' => $selected)); 
                ?> 
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'created'); ?>
		<?php //echo $form->textField($model,'created'); ?>
		<?php //echo $form->error($model,'created'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'is_online'); ?>
                <?php 
                        $selected = 'N';
                        if(isset($model->is_online) && !empty($model->is_online)){
                            $selected = $model->is_online;
                        }
                
                        echo $form->dropDownList($model,'is_online', Devices::model()->online_options, array('selected' => $selected)); 
                ?> 
		<?php echo $form->error($model,'is_online'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_checked'); ?>
		<?php echo $form->textField($model,'last_checked'); ?>
		<?php echo $form->error($model,'last_checked'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'os'); ?>
		<?php echo $form->textArea($model,'os',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'os'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'arch'); ?>
                <?php 
                        $selected = '';
                        if(isset($model->arch) && !empty($model->arch)){
                            $selected = $model->arch;
                        }
                
                        echo $form->dropDownList($model,'arch', Devices::model()->arch_options, array('selected' => $selected)); 
                ?> 
		<?php echo $form->error($model,'arch'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'php_info'); ?>
		<?php echo $form->textArea($model,'php_info',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'php_info'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'comment'); ?>
		<?php echo $form->textArea($model,'comment',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'comment'); ?>
	</div>

	<div class="row buttons">
		<?php //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
                <?php echo TbHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->