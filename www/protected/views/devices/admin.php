<?php
/* @var $this DevicesController */
/* @var $model Devices */

$this->breadcrumbs=array(
	'Devices'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Devices', 'url'=>array('index')),
	array('label'=>'Create Devices', 'url'=>array('create')),
        array('label'=>'Upload Devices', 'url'=>array('import')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#devices-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Devices</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php //$this->widget('zii.widgets.grid.CGridView', array(
        $this->widget('\TbGridView',array(
	'id'=>'devices-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		//'ip',
                array('name'=>'ip','type'=>'raw','value'=>'$data->getDeviceConnectorsText()'),
		//'port',
		'type',
		'created',
		//'is_online',
                array('name'=>'is_online',
                    'type'=>'raw',
                    'value'=>'($data->is_online == \'Y\' ? \'<span class="label label-success">Online</span>\' : \'<span class="label label-default">Offline</span>\')',
                    ),
		'last_checked',
		'os',
		'arch',
		//'php_info',
		'comment',
		/*
		array(
			'class'=>'CButtonColumn',
		),
                */
                array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
