<?php
/* @var $this HostsController */
/* @var $model Hosts */


$this->breadcrumbs=array(
	'Hosts'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Hosts', 'url'=>array('index')),
	array('label'=>'Create Hosts', 'url'=>array('create')),
        array('label'=>'Upload Hosts', 'url'=>array('import')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#hosts-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Hosts</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('\TbGridView',array(
	'id'=>'hosts-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		//'hostname',
                array('name'=>'hostname','type'=>'raw','value'=>'$data->getHostsConnectorsText()'),
                array(
                    'name'=>'id_device',
                    'value'=>'$data->devices_table->ip',
                ),
		'created',
                array(
                    'name'=>'is_online',
                    'type'=>'raw',
                    //'value'=>'$data["is_online"]=="Y"?"<span class="badge">Online</span>":"<span class="badge">Offline</span>"',
                    'value'=>'($data->is_online == \'Y\' ? \'<span class="label label-success">Online</span>\' : \'<span class="label label-default">Offline</span>\')',
                ),
		'last_checked',
		/*
		'php_info',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>