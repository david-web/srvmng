<?php
$this->breadcrumbs=array(
	'Hosts'=>array('index'),
	'Import Hosts',
);

$this->menu=array(
	array('label'=>'List Hosts', 'url'=>array('index')),
	array('label'=>'Create Hosts', 'url'=>array('create')),
	array('label'=>'Manage Hosts', 'url'=>array('admin')),
);
?>

<div class="form">
    <?php $form=$this->beginWidget('\TbActiveForm', array(
	'id'=>'import-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
        //'layout' => TbHtml::FORM_LAYOUTHORIZONTAL,
    )); ?>
     
    <fieldset>
     
        <legend>Import Hosts</legend>
        <?php
                if(isset($host_error)){
                    echo TbHtml::alert(TbHtml::ALERT_COLOR_DANGER,
                    '<strong>'.$host_error.'!</strong> Change a few things up and try submitting again.');
                }
                elseif(isset($import_log)){
                    echo TbHtml::alert(TbHtml::ALERT_COLOR_SUCCESS,
                    '<strong>'.$import_log.'!</strong>');
                }
        ?>
        <?php //echo $form->labelEx($model,'csv_file'); ?>
        <?php echo $form->fileFieldControlGroup($model, 'csv_file'); ?>
        <?php echo $form->error($model, 'csv_file'); ?>
     
    </fieldset>
    
    <div class="form-actions">
        <?php echo TbHtml::formActions(array(
            TbHtml::submitButton('Upload File', array('color' => TbHtml::BUTTON_COLOR_PRIMARY)),
            TbHtml::resetButton('Reset'),
        )); ?>

        <?php echo $form->errorSummary($model); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>