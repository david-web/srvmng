<?php
/* @var $this HostsController */
/* @var $data Hosts */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hostname')); ?>:</b>
	<?php echo CHtml::encode($data->hostname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_device')); ?>:</b>
	<?php echo CHtml::encode($data->id_device); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_online')); ?>:</b>
	<?php echo CHtml::encode($data->is_online); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_checked')); ?>:</b>
	<?php echo CHtml::encode($data->last_checked); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('php_info')); ?>:</b>
	<?php echo CHtml::encode($data->php_info); ?>
	<br />


</div>