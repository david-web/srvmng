<?php
/* @var $this HostsController */
/* @var $model Hosts */
?>

<?php
$this->breadcrumbs=array(
	'Hosts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Hosts', 'url'=>array('index')),
	array('label'=>'Manage Hosts', 'url'=>array('admin')),
);
?>

<h1>Create Hosts</h1>

<?php 
$data=array();
$data['model'] = $model;
if(isset($host_error) && !empty($host_error)){
    $data['host_error'] = $host_error;
}

$this->renderPartial('_form', $data); 
?>