<?php
/* @var $this HostsController */
/* @var $model Hosts */
?>

<?php
$this->breadcrumbs=array(
	'Hosts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Hosts', 'url'=>array('index')),
	array('label'=>'Create Hosts', 'url'=>array('create')),
	array('label'=>'Update Hosts', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Hosts', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Hosts', 'url'=>array('admin')),
);
?>

<h1>View Hosts #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id',
		'hostname',
                array
                (
                        'name'=>'id_device',
                        'type'=>'raw',
                        'value'=>$model->devices_table->ip,
                ),
		'created',
                array
                (
                        'name'=>'is_online',
                        'type'=>'raw',
                        'value'=>$model["is_online"]=="Y"?"Online":"Offline",
                ),
		'last_checked',
		'php_info',
	),
)); ?>