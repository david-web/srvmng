<?php
/* @var $this HostsController */
/* @var $model Hosts */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('\TbActiveForm', array(
	'id'=>'hosts-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

            
            <?php 
                if(isset($host_error)){
                    
                    echo TbHtml::alert(TbHtml::ALERT_COLOR_DANGER,
                    '<strong>'.$host_error.'!</strong> Change a few things up and try submitting again.');
                    
                    /*
                    Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER,
                    '<strong>'.$host_error.'!</strong> Change a few things up and try submitting again.');
                    */
                }
                
                echo $form->textAreaControlGroup($model,'hostname',array('rows'=>2,'span'=>8)); 
            ?>

            <?php //echo $form->textFieldControlGroup($model,'id_device',array('span'=>5,'maxlength'=>20)); ?>

            <?php //echo $form->textFieldControlGroup($model,'created',array('span'=>5)); ?>

            <?php echo $form->dropDownList($model,'is_online', array('Y' => 'Online', 'N' => 'Offline')); ?>

            <?php //echo $form->textFieldControlGroup($model,'last_checked',array('span'=>5)); ?>

            <?php echo $form->textAreaControlGroup($model,'php_info',array('rows'=>6,'span'=>8)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->