<?php
/* @var $this HostsController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Hosts',
);

$this->menu=array(
	array('label'=>'Create Hosts','url'=>array('create')),
	array('label'=>'Manage Hosts','url'=>array('admin')),
);
?>

<h1>Hosts</h1>

<?php $this->widget('\TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>