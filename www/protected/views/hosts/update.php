<?php
/* @var $this HostsController */
/* @var $model Hosts */
?>

<?php
$this->breadcrumbs=array(
	'Hosts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Hosts', 'url'=>array('index')),
	array('label'=>'Create Hosts', 'url'=>array('create')),
	array('label'=>'View Hosts', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Hosts', 'url'=>array('admin')),
);
?>

    <h1>Update Hosts <?php echo $model->id; ?></h1>

<?php 
$data=array();
$data['model'] = $model;
if(isset($host_error) && !empty($host_error)){
    $data['host_error'] = $host_error;
}

$this->renderPartial('_form', $data); 
?>