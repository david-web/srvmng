<?php
/* @var $this CommandsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Commands',
);

$this->menu=array(
	array('label'=>'Create Commands', 'url'=>array('create')),
	array('label'=>'Manage Commands', 'url'=>array('admin')),
);
?>

<h1>Commands</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
