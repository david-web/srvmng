<?php
/* @var $this CommandsController */
/* @var $model Commands */

$this->breadcrumbs=array(
	'Commands'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Commands', 'url'=>array('index')),
	array('label'=>'Create Commands', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#commands-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Commands</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php //$this->widget('zii.widgets.grid.CGridView', array(
        $this->widget('\TbGridView', array(
	'id'=>'commands-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		//'type',
                array('name'=>'type',
                    'type'=>'raw',
                    'value'=>'$data->type_options[$data->type]',
                ),
		'param',
                'run_as_adm',
		'created',
		//'created_by',
                array('name'=>'created_by',
                    'type'=>'raw',
                    'value'=>'$data->createdBy->username',
                ),
		/*
		'updated',
		'updated_by',
		*/
                /*
                'link'=>array(
                        'header'=>'Go to...',
                        'type'=>'raw',
                        'value'=> 'CHtml::button("button label",array("onclick"=>"document.location.href=\'".Yii::app()->controller->createUrl("controller/action",array("id"=>$data->id))."\'"))',
                ),
                */ 
		array(
			//'class'=>'CButtonColumn',
                        'class'=>'bootstrap.widgets.TbButtonColumn',
                        'template' => '{view} {update} {delete} {launch}',
                        'buttons'=>array(
                                'launch' => array(
                                        'label'=>'Launch', // text label of the button
                                        'icon'=>'play',
                                        'url'=>"CHtml::normalizeUrl(array('launch', 'id'=>\$data->id))",
                                        //'imageUrl'=>'/path/to/copy.gif',  // image URL of the button. If not set or false, a text link is used
                                        'options' => array('class'=>'launch'), // HTML options for the button
                                        'visible'=>'true',
                                ),
                        ),

		),
	),
)); ?>
