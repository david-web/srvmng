<?php
/* @var $this CommandsController */
/* @var $model Commands */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'commands-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>120)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
                <?php 
                        $selected = 'D';
                        if(isset($model->type) && !empty($model->type)){
                            $selected = $model->type;
                        }
                
                        echo $form->dropDownList($model,'type', Commands::model()->type_options, array('selected' => $selected)); 
                ?> 
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'param'); ?>
		<?php echo $form->textArea($model,'param',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'param'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'select_conn'); ?>
                <br>
                <?php
                
                    $connectors_list=CHtml::listData(
                            Connectors::model()->findAll(),
                            'id',
                            'name'
                    );
                    
                    $selected_Array=array();
                    if(isset($com_conns)){
                        foreach($com_conns as $com_conn){
                            $selected_Array[]=$com_conn->idConnector->id;
                        }
                    }
                
                    echo CHtml::checkBoxList('select_conn', $selected_Array, $connectors_list);
                    
                    
                ?> 
		<?php echo $form->error($model,'select_conn'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'run_as_adm'); ?>
                <br>
                <?php
                    
                    $is_checked = false;
                    $run_as_adm_val = 'off';
                    if($model->run_as_adm == 'Y')
                    {
                        $is_checked = true;
                        $run_as_adm_val = 'on';
                    }
                    
                    echo CHtml::CheckBox('run_as_adm',$is_checked, array ('value'=>$run_as_adm_val,));
                ?> 
		<?php echo $form->error($model,'run_as_adm'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->