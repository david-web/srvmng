<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<div id='content' class='row'>
    <div class='element1 col-md-2 sidebar'>
            <h3>Operations:</h3>
            <?php
                //array_unshift($this->menu , array('label' => 'Operations:'));
                //echo TbHtml::navList($this->menu); 
                echo TbHtml::stackedPills($this->menu);
                
            ?>
    </div>
    <div class='element2 col-md-10 main'>
            <?php echo $content; ?> 
    </div>
    <!--
    <div class='span2 sidebar'>
        <h3>Right Sidebar</h3>
        <ul class="nav nav-tabs nav-stacked">
          <li><a href='#'>Another Link 1</a></li>
          <li><a href='#'>Another Link 2</a></li>
          <li><a href='#'>Another Link 3</a></li>
        </ul>
    </div>
    -->
</div>

<?php $this->endContent(); ?>
