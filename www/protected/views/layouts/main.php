<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">
        
        <!-- bootstrap framework -->
        <?php Yii::app()->bootstrap->register(); ?>
        
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">
    
	<div id="mainmenu">
                <?php 
                
                $controller = Yii::app()->controller->id;
                
                $items_arr =  
                array(
                        array('label'=>'Home', 'url'=>array('/site/index')),
                        array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
                        array('label'=>'Contact', 'url'=>array('/site/contact')),
                        array('label'=>'Users', 'url'=>array('/users'), 'visible'=>Yii::app()->user->role == 'admin'),
                        array('label'=>'Devices', 'url'=>array('/devices'), 'visible'=>!Yii::app()->user->isGuest),
                        array('label'=>'Hosts', 'url'=>array('/hosts'), 'visible'=>!Yii::app()->user->isGuest),
                        array('label'=>'Commands', 'url'=>array('/commands'), 'visible'=>!Yii::app()->user->isGuest),
                        array('label'=>'ExecLog', 'url'=>array('/execLog'), 'visible'=>!Yii::app()->user->isGuest),
                        array('label'=>'Connectors', 'url'=>array('/connectors'), 'visible'=>!Yii::app()->user->isGuest),
                        array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                        array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
                );
                
                for($i=0;$i<count($items_arr);$i++){
                    if(strtolower($items_arr[$i]['label']) == $controller){
                        $items_arr[$i]['active'] = true;
                        break;
                    }
                }
                
                $this->widget('bootstrap.widgets.TbNavbar', array(
                    //'color' => TbHtml::NAVBAR_COLOR_INVERSE,
                    'brandLabel' => CHtml::encode("DMF"),
                    'collapse' => true,
                    'display' => null, // default is static to top
                    'items' => array(
                        array(
                            'class' => 'bootstrap.widgets.TbNav',
                            'items' => $items_arr,
                        ),
                    ),
                )); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
                <?php 
                    $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
                        ));
                    //var_dump($this->breadcrumbs);die();
                    //echo TbHtml::breadcrumbs($this->breadcrumbs);
                ?>
                <!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer" class="text-center">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->
        <?php 
            if(defined('IS_DEV')){
                echo '<div class="clear"></div>';
                $debug = "Debug info: <br>";
                $debug .= "user_id: ".Yii::app()->user->id."<br>";
                $debug .= "user_role: ".Yii::app()->user->role."<br>";
                
                echo TbHtml::well($debug);
            }
        ?>

</div><!-- page -->

</body>
</html>
