<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="span-19">
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<div class="span-5 last">
	<div id="sidebar">
	<?php
		/*
                $this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Operations',
		));
		$this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			'htmlOptions'=>array('class'=>'operations'),
		));
		$this->endWidget();
                */
        ?>
            <div class="well" style="max-width: 340px; padding: 8px 0;">
                <?php 
                /* //-- example menu
                    echo TbHtml::navList(array(
                        array('label' => 'List header'),
                        array('label' => 'Home', 'url' => '#', 'active' => true),
                        array('label' => 'Library', 'url' => '#'),
                        array('label' => 'Applications', 'url' => '#'),
                        array('label' => 'Another list header'),
                        array('label' => 'Profile', 'url' => '#'),
                        array('label' => 'Settings', 'url' => '#'),
                        TbHtml::menuDivider(),
                        array('label' => 'Help', 'url' => '#'),
                    )); 
                */
                    array_unshift($this->menu , array('label' => 'Operations:'));
                    echo TbHtml::navList($this->menu); 
                ?>
            </div>
	</div><!-- sidebar -->
</div>
<?php $this->endContent(); ?>