pushd "C:\Program Files\RabbitMQ Server\rabbitmq_server-3.7.3\sbin\"

start rabbitmqctl.bat stop_app
ping 127.0.0.1 -n 8 > nul 
start rabbitmqctl.bat reset    # Be sure you really want to do this!
ping 127.0.0.1 -n 6 > nul
start rabbitmqctl.bat start_app

start rabbitmqctl.bat list_queues
pause

