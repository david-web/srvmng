-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.25 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table srvmng.commands
DROP TABLE IF EXISTS `commands`;
CREATE TABLE IF NOT EXISTS `commands` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Command ID',
  `name` varchar(120) COLLATE utf8_bin NOT NULL COMMENT 'Name',
  `type` enum('D','E') COLLATE utf8_bin NOT NULL DEFAULT 'E' COMMENT 'Type',
  `run_as_adm` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  `param` text COLLATE utf8_bin NOT NULL COMMENT 'Param',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created',
  `created_by` bigint(20) DEFAULT NULL COMMENT 'Created By',
  `updated` datetime DEFAULT NULL COMMENT 'Updated',
  `updated_by` bigint(20) DEFAULT NULL COMMENT 'Updated By',
  `is_running` enum('Y','N') COLLATE utf8_bin DEFAULT 'N' COMMENT 'Is Running',
  `started` datetime DEFAULT NULL COMMENT 'Started',
  `finished` datetime DEFAULT NULL COMMENT 'Finished',
  PRIMARY KEY (`id`),
  KEY `user` (`created_by`),
  KEY `user2` (`updated_by`),
  CONSTRAINT `user` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `user2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table srvmng.command_connectors
DROP TABLE IF EXISTS `command_connectors`;
CREATE TABLE IF NOT EXISTS `command_connectors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_command` bigint(20) NOT NULL DEFAULT '0',
  `id_connector` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `command` (`id_command`),
  KEY `connector_table` (`id_connector`),
  CONSTRAINT `command` FOREIGN KEY (`id_command`) REFERENCES `commands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `connector_table` FOREIGN KEY (`id_connector`) REFERENCES `connectors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table srvmng.command_lists
DROP TABLE IF EXISTS `command_lists`;
CREATE TABLE IF NOT EXISTS `command_lists` (
  `id` bigint(20) NOT NULL,
  `id_command` bigint(20) NOT NULL,
  `type` enum('D','H') COLLATE utf8_bin NOT NULL,
  `id_list` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `command4` (`id_command`),
  CONSTRAINT `command4` FOREIGN KEY (`id_command`) REFERENCES `commands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table srvmng.connectors
DROP TABLE IF EXISTS `connectors`;
CREATE TABLE IF NOT EXISTS `connectors` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Connector ID',
  `name` varchar(120) COLLATE utf8_bin NOT NULL COMMENT 'Name',
  `type` enum('http','backhttp','ssh','telnet') COLLATE utf8_bin NOT NULL DEFAULT 'http' COMMENT 'Type',
  PRIMARY KEY (`id`),
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table srvmng.connector_http_params
DROP TABLE IF EXISTS `connector_http_params`;
CREATE TABLE IF NOT EXISTS `connector_http_params` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `connector_id` int(11) NOT NULL DEFAULT '0',
  `proto` enum('http','https') COLLATE utf8_bin NOT NULL DEFAULT 'http' COMMENT 'Protocol',
  `method` enum('get','post','cookie','header') COLLATE utf8_bin NOT NULL DEFAULT 'header' COMMENT 'Method',
  `path` text COLLATE utf8_bin NOT NULL COMMENT 'Path',
  `post_data` text COLLATE utf8_bin COMMENT 'Params',
  `cookie` text COLLATE utf8_bin,
  `headers` text COLLATE utf8_bin,
  `has_admin_access` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  `admin_wrapper` text COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  KEY `connector3` (`connector_id`),
  CONSTRAINT `connector3` FOREIGN KEY (`connector_id`) REFERENCES `connectors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table srvmng.connector_ssh_params
DROP TABLE IF EXISTS `connector_ssh_params`;
CREATE TABLE IF NOT EXISTS `connector_ssh_params` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_device` bigint(20) NOT NULL DEFAULT '0',
  `login` varchar(120) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pass` varchar(120) COLLATE utf8_bin DEFAULT '',
  `key` text COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  KEY `device_key` (`id_device`),
  CONSTRAINT `device_key` FOREIGN KEY (`id_device`) REFERENCES `devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6798 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table srvmng.connector_telnet_params
DROP TABLE IF EXISTS `connector_telnet_params`;
CREATE TABLE IF NOT EXISTS `connector_telnet_params` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_device` bigint(20) NOT NULL DEFAULT '0',
  `login` varchar(120) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pass` varchar(120) COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `device_key` (`id_device`),
  CONSTRAINT `connector_telnet_params_ibfk_1` FOREIGN KEY (`id_device`) REFERENCES `devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table srvmng.devices
DROP TABLE IF EXISTS `devices`;
CREATE TABLE IF NOT EXISTS `devices` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Device ID',
  `ip` varchar(30) COLLATE utf8_bin NOT NULL COMMENT 'IP address',
  `port` int(11) DEFAULT '80' COMMENT 'Port',
  `type` enum('server','pc','router','cctv') COLLATE utf8_bin DEFAULT NULL COMMENT 'Type',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created',
  `is_online` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N' COMMENT 'Online',
  `last_checked` datetime DEFAULT NULL COMMENT 'Last Checked',
  `os` text COLLATE utf8_bin COMMENT 'OS',
  `arch` enum('arm','x64','x86','mipsbe','mipsle','powerpc') COLLATE utf8_bin DEFAULT NULL COMMENT 'Arch',
  `php_info` text COLLATE utf8_bin COMMENT 'PHP_INFO',
  `comment` text COLLATE utf8_bin COMMENT 'Comment',
  PRIMARY KEY (`id`),
  KEY `ip` (`ip`),
  KEY `port` (`port`),
  KEY `type` (`type`),
  KEY `is_online` (`is_online`),
  KEY `arch` (`arch`)
) ENGINE=InnoDB AUTO_INCREMENT=23570 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table srvmng.devices_connectors
DROP TABLE IF EXISTS `devices_connectors`;
CREATE TABLE IF NOT EXISTS `devices_connectors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_device` bigint(20) NOT NULL DEFAULT '0',
  `id_connector` int(11) NOT NULL DEFAULT '0',
  `port` int(11) DEFAULT '80',
  PRIMARY KEY (`id`),
  KEY `devices` (`id_device`),
  KEY `connector` (`id_connector`),
  CONSTRAINT `connector` FOREIGN KEY (`id_connector`) REFERENCES `connectors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `devices` FOREIGN KEY (`id_device`) REFERENCES `devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40865 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table srvmng.devices_lists
DROP TABLE IF EXISTS `devices_lists`;
CREATE TABLE IF NOT EXISTS `devices_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'List ID',
  `name` varchar(120) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT 'Name',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created',
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table srvmng.devices_lists_val
DROP TABLE IF EXISTS `devices_lists_val`;
CREATE TABLE IF NOT EXISTS `devices_lists_val` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_device` bigint(20) NOT NULL DEFAULT '0',
  `id_device_list` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `device` (`id_device`),
  KEY `device_list` (`id_device_list`),
  CONSTRAINT `device` FOREIGN KEY (`id_device`) REFERENCES `devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `device_list` FOREIGN KEY (`id_device_list`) REFERENCES `devices_lists` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table srvmng.devices_tags
DROP TABLE IF EXISTS `devices_tags`;
CREATE TABLE IF NOT EXISTS `devices_tags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_device` bigint(20) NOT NULL DEFAULT '0',
  `id_tag` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `devicefk1` (`id_device`),
  KEY `tagfk1` (`id_tag`),
  CONSTRAINT `devicefk1` FOREIGN KEY (`id_device`) REFERENCES `devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tagfk1` FOREIGN KEY (`id_tag`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table srvmng.exec_log
DROP TABLE IF EXISTS `exec_log`;
CREATE TABLE IF NOT EXISTS `exec_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `id_command` bigint(20) NOT NULL DEFAULT '0' COMMENT 'Command',
  `id_obj` bigint(20) NOT NULL DEFAULT '0',
  `obj_type` enum('D','H') COLLATE utf8_bin NOT NULL DEFAULT 'D',
  `id_connector` int(11) NOT NULL DEFAULT '0' COMMENT 'Connector',
  `result` text COLLATE utf8_bin COMMENT 'REsult',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Timestamp',
  PRIMARY KEY (`id`),
  KEY `id_obj` (`id_obj`),
  KEY `id_command` (`id_command`),
  KEY `id_connector` (`id_connector`),
  CONSTRAINT `command_table` FOREIGN KEY (`id_command`) REFERENCES `commands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `connector_table3` FOREIGN KEY (`id_connector`) REFERENCES `connectors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34845 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table srvmng.hosts
DROP TABLE IF EXISTS `hosts`;
CREATE TABLE IF NOT EXISTS `hosts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Host ID',
  `hostname` text COLLATE utf8_bin NOT NULL COMMENT 'Hostname',
  `id_device` bigint(20) NOT NULL COMMENT 'Device',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created',
  `is_online` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N' COMMENT 'Online',
  `last_checked` datetime DEFAULT NULL COMMENT 'Last Checked',
  `php_info` text COLLATE utf8_bin COMMENT 'PHP_INFO',
  PRIMARY KEY (`id`),
  KEY `id_device` (`id_device`),
  KEY `is_online` (`is_online`),
  CONSTRAINT `devices_table` FOREIGN KEY (`id_device`) REFERENCES `devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table srvmng.hosts_connectors
DROP TABLE IF EXISTS `hosts_connectors`;
CREATE TABLE IF NOT EXISTS `hosts_connectors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_host` bigint(20) NOT NULL DEFAULT '0',
  `id_connector` int(11) NOT NULL DEFAULT '0',
  `port` int(11) DEFAULT '80',
  PRIMARY KEY (`id`),
  KEY `hosts` (`id_host`),
  KEY `connectors` (`id_connector`),
  CONSTRAINT `connectors` FOREIGN KEY (`id_connector`) REFERENCES `connectors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `hosts` FOREIGN KEY (`id_host`) REFERENCES `hosts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table srvmng.hosts_lists
DROP TABLE IF EXISTS `hosts_lists`;
CREATE TABLE IF NOT EXISTS `hosts_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'List ID',
  `name` varchar(120) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT 'Name',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created',
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table srvmng.hosts_list_val
DROP TABLE IF EXISTS `hosts_list_val`;
CREATE TABLE IF NOT EXISTS `hosts_list_val` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_host` bigint(20) NOT NULL DEFAULT '0',
  `id_host_list` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `host` (`id_host`),
  KEY `host_list` (`id_host_list`),
  CONSTRAINT `host` FOREIGN KEY (`id_host`) REFERENCES `hosts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `host_list` FOREIGN KEY (`id_host_list`) REFERENCES `hosts_lists` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table srvmng.hosts_tags
DROP TABLE IF EXISTS `hosts_tags`;
CREATE TABLE IF NOT EXISTS `hosts_tags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_host` bigint(20) NOT NULL DEFAULT '0',
  `id_tag` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `hostfk3` (`id_host`),
  KEY `tagfk3` (`id_tag`),
  CONSTRAINT `hostfk3` FOREIGN KEY (`id_host`) REFERENCES `hosts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tagfk3` FOREIGN KEY (`id_tag`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table srvmng.logins
DROP TABLE IF EXISTS `logins`;
CREATE TABLE IF NOT EXISTS `logins` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_obj` bigint(20) NOT NULL DEFAULT '0',
  `type_obj` enum('D','H') COLLATE utf8_bin NOT NULL DEFAULT 'D',
  `type_service` enum('vesta_adm','mysql') COLLATE utf8_bin NOT NULL DEFAULT 'vesta_adm',
  `login` text COLLATE utf8_bin NOT NULL,
  `pass` text COLLATE utf8_bin NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_obj` (`id_obj`),
  KEY `type_obj` (`type_obj`),
  KEY `type_service` (`type_service`)
) ENGINE=InnoDB AUTO_INCREMENT=6793 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table srvmng.tags
DROP TABLE IF EXISTS `tags`;
CREATE TABLE IF NOT EXISTS `tags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_bin NOT NULL COMMENT 'Name',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table srvmng.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'User ID',
  `username` varchar(128) COLLATE utf8_bin NOT NULL COMMENT 'Username',
  `password` varchar(128) COLLATE utf8_bin NOT NULL COMMENT 'Password',
  `email` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT 'Email',
  `role` enum('operator','admin') COLLATE utf8_bin NOT NULL DEFAULT 'operator' COMMENT 'Role',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created',
  `is_active` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'Y' COMMENT 'Active',
  PRIMARY KEY (`id`),
  KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table srvmng.yiisession
DROP TABLE IF EXISTS `yiisession`;
CREATE TABLE IF NOT EXISTS `yiisession` (
  `id` char(32) COLLATE utf8_bin NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
