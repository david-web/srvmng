$(document).ready(function(){
        /*
        $('#add_connector').on('click', function() {
            var div = document.getElementById('connector_div'),
            clone = div.cloneNode(true); // true means clone all childNodes and all event handlers
            //clone.id = "connector_divn";
            //clone.on_click = "connector_divn";
            $('#connectors').append(clone);
        });
        */
       
        
        $('#add_connector').on('click', function() {
            
            var con_index = $("#connectors > div").length;
            console.log('con_index'+con_index);
            
            
            var last_con_index = $("#connectors").children().last().data('id');
            if(last_con_index === undefined){
                last_con_index = 0;
            }
            console.log('last_con_index'+last_con_index);
            
            var count = $("#connectors > div").length;
            
            var data = 'connectors_count='+count+'&con_index='+con_index+'&last_con_index='+last_con_index;
            
            $.ajax({
                    url: 'index.php?r=devices/GetConnectorDetails',
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    success: function(data){
                        //alert(data.html);
                        //elem.parent.append(data.html);
                        $('#connectors').append(data.html);
                        //console.log(elem);
                    },
                    fail:function(sender, message, details){
                            alert("Sorry, something went wrong!");
                    }
            });

        });
        
       
        $(document).on("change",".get_con_detail", function () {
            //console.log('Devices_connector changed');
            //alert( this.value );
            var id_connector = this.value;
            var elem = $(this);
            
            var con_index = elem.data("id"); 
            //console.log("con_index"+con_index);
            
            var count = $("#connectors > div").length;
            var data = 'id_connector='+id_connector+'&con_index='+con_index;
            
            var con_obj = $(this).data('con_obj');
            if(con_obj !== undefined){
                data = data + '&con_obj='+con_obj;
                //console.log('con_obj'+con_obj);
            }
            
            $.ajax({
                    url: 'index.php?r=devices/GetConnectorDetails',
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    success: function(data){
                        //alert(data.html);
                        //elem.parent.append(data.html);
                        elem.closest('div').html(data.html);
                        //console.log(elem);
                    },
                    fail:function(sender, message, details){
                            alert("Sorry, something went wrong!");
                    }
            });
        });
        

});

function remove_connector1(obj){
    //console.log('remove_connector called');
    var con_obj = $(obj).data('con_obj');
    //console.log('con_obj'+con_obj);
    
    if(con_obj !== undefined){
        var data = 'con_obj='+con_obj;
        //console.log('con_obj'+con_obj);
        
        $.ajax({
                url: 'index.php?r=devices/DeleteConnector',
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function(data){
                    if(data.ok === 1){
                        $(obj).parent().remove();
                        $('#connectors').prepend(data.html)
                        return;
                    }
                },
                fail:function(sender, message, details){
                        alert("Sorry, something went wrong!");
                        return;
                }
        });
    }
    
    $(obj).parent().remove();
}

